package com.gcreate.treatrip

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.gcreate.treatrip.tools.Global

class WelcomePageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)


        Global.getWindowSize(this)
        Global.initTreatripAPI()

        // 跳轉
        Handler().postDelayed({
            startActivity(Intent(applicationContext , MainActivity::class.java))
            finish()
        } , 1000)
    }


}