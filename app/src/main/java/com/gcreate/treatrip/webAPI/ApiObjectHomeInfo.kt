package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectHomeInfo(
    @SerializedName("limited_time")
    val limited_time: List<LimitedTime>,
    @SerializedName("popular_attractions")
    val popular_attractions: List<PopularAttraction>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("banner_img")
    val banner_img: List<String>
)

data class LimitedTime(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)

data class PopularAttraction(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)