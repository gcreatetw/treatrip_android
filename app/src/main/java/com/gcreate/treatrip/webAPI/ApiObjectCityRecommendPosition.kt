package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

//  首頁點城市進入後的景點推薦用的API Model
data class ApiObjectCityRecommendPosition(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("towns")
    val towns: List<Town>,
    @SerializedName("region_banner_img")
    val region_banner_img : String
)

data class Town(
    @SerializedName("Recommend_Attractions")
    val Recommend_Attractions: List<RecommendAttraction>,
    @SerializedName("township")
    val township: String
)

data class RecommendAttraction(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String
)