package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectLoadMoreArticle(
    @SerializedName("posts")
    val posts: MutableList<Article>,
    @SerializedName("success")
    val success: Boolean
)

data class Article(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)