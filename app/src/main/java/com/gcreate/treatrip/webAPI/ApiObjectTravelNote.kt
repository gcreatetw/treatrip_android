package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectTravelNote(
    @SerializedName("blogger")
    val blogger: List<Blogger>,
    @SerializedName("playmate")
    val playmate: List<Playmate>,
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("travel_recommend")
    val travel_recommend: List<TravelRecommend>
)

data class Blogger(
    @SerializedName("ID")
    val ID: String,
    @SerializedName("bloger_id")
    val bloger_id: String,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("blogger_job_title")
    val blogger_job_title: String
)

data class Playmate(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)

//data class TravelRecommend(
//    @SerializedName("ID")
//    val ID: Int,
//    @SerializedName("post_img")
//    val post_img: String,
//    @SerializedName("post_title")
//    val post_title: String
//)