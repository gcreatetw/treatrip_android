package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectGoWhere(
    @SerializedName("city_posts")
    val city_posts: List<CityPost>,
    @SerializedName("posts")
    val posts: List<Post>,
    @SerializedName("success")
    val success: Boolean
)

data class CityPost(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)

data class Post(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)