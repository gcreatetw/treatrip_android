package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectLoadMoreArticleCategory(
    @SerializedName("categories")
    val categories: List<Category>,
    @SerializedName("success")
    val success: Boolean
)

data class Category(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)