package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectTravelRecommend(
    @SerializedName("success")
    val success: Boolean,
    @SerializedName("travel_recommend")
    val travel_recommend: List<TravelRecommend>
)

data class TravelRecommend(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)