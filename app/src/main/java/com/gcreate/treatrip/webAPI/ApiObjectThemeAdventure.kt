package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

//  主題冒險
data class ApiObjectThemeAdventure(
    @SerializedName("adventure")
    val adventure: MutableList<Adventure>,
    @SerializedName("success")
    val success: Boolean
)

data class Adventure(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)