package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectTaiwanExplore(
    @SerializedName("BN")
    val BN: String,
    @SerializedName("discover_taiwain")
    val discover_taiwain: MutableList<DiscoverTaiwain>,
    @SerializedName("success")
    val success: Boolean
)

data class DiscoverTaiwain(
    @SerializedName("catgories")
    val catgories: MutableList<Catgory>,
    @SerializedName("main_type")
    val main_type: String,
    @SerializedName("icon")
    val icon : String
)

data class Catgory(
    @SerializedName("cat_posts")
    val cat_posts: MutableList<CatPost>,
    @SerializedName("cat_title")
    val cat_title: String
)

data class CatPost(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String
)