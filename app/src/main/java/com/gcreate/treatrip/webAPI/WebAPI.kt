package com.gcreate.treatrip.webAPI

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface WebAPI {

    companion object {

        private const val DOMAIN = "www.treatrip.com/wp-json/treatrip_api"

        //        String SERVER_IP = "https://" + TEST_HOST + ":" + TEST_PORT + "/api/";
        //String SERVER_IP = "https://app.right-time.com.tw:443/api"  ;
        val TREATRIP_SERVER = "https://$DOMAIN/"

        
    }

    @Headers(*["Content-Type: application/json"])
    @POST("home_page")
    fun getHomeInfo(): Call<ApiObjectHomeInfo>

    @Headers(*["Content-Type: application/json"])
    @POST("travel_notes_page")
    fun getTravelNoteInfo(): Call<ApiObjectTravelNote>

    @Headers(*["Content-Type: application/json"])
    @POST("travel_recommend")
    fun getTravelRecommendInfo(@Body requestBody: RequestBody): Call<ApiObjectTravelRecommend>

    //  鄉鎮市區推薦景點
    @Headers(*["Content-Type: application/json"])
    @POST("region")
    fun getCityRecommendPosition(@Body requestBody: RequestBody): Call<ApiObjectCityRecommendPosition>

    //  單一景點詳細資訊
    @Headers(*["Content-Type: application/json"])
    @POST("post_info")
    fun getAttractionInfo(@Body requestBody: RequestBody): Call<ApiObjectAttractionInfo>

    //  主題冒險
    @Headers(*["Content-Type: application/json"])
    @POST("adventure")
    fun getThemeAdventure(@Body requestBody: RequestBody): Call<ApiObjectThemeAdventure>

    //  探索台灣
    @Headers(*["Content-Type: application/json"])
    @POST("discover_taiwain")
    fun getTaiwanExploreInfo(): Call<ApiObjectTaiwanExplore>

    //  部落客資訊
    @Headers(*["Content-Type: application/json"])
    @POST("blog_posts")
    fun getBloggerInfo(@Body requestBody: RequestBody): Call<ApiObjectBlogger>

    //  部落客資訊
    @Headers(*["Content-Type: application/json"])
    @POST("recommend_scenic_spot")
    fun getRecommendUploadResult(@Body requestBody: RequestBody): Call<ApiObjectRecommendUploadResult>

    //  想趣哪裡
    @Headers(*["Content-Type: application/json"])
    @POST("advanced_search")
    fun getGoWhereResult(@Body requestBody: RequestBody): Call<ApiObjectGoWhere>


    //  更多文章，文章分類
    @Headers(*["Content-Type: application/json"])
    @POST("post_categories")
    fun getArticleCategory(): Call<ApiObjectLoadMoreArticleCategory>

    //  更多文章，文章內文
    @Headers(*["Content-Type: application/json"])
    @POST("category_post")
    fun getLoadMoreArticle(@Body requestBody: RequestBody): Call<ApiObjectLoadMoreArticle>

}