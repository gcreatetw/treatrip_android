package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectRecommendUploadResult(
    @SerializedName("message")
    val message: String,
    @SerializedName("success")
    val success: Boolean
)