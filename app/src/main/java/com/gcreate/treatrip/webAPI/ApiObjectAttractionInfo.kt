package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

//  首頁景點用的API Model
data class ApiObjectAttractionInfo(
    @SerializedName("post_info")
    val post_info: PostInfo,
    @SerializedName("success")
    val success: Boolean
)

data class PostInfo(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("add")
    val add: String,
    @SerializedName("cat")
    val cat: List<String>,
    @SerializedName("img_form")
    val img_form: String,
    @SerializedName("opentime")
    val opentime: String,
    @SerializedName("post_content")
    val post_content: String,
    @SerializedName("post_date")
    val post_date: String,
    @SerializedName("post_img")
    val post_img: List<PostImg>,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("px")
    val px: String,
    @SerializedName("py")
    val py: String,
    @SerializedName("region")
    val region: String,
    @SerializedName("tag")
    val tag: List<String>,
    @SerializedName("tel")
    val tel: String,
    @SerializedName("town")
    val town: String,
    @SerializedName("travellinginfo")
    val travellinginfo: String,
    @SerializedName("website")
    val website: String
)

data class PostImg(
    @SerializedName("picdescribe")
    val picdescribe: String,
    @SerializedName("picture")
    val picture: String
)