package com.gcreate.treatrip.webAPI

import com.google.gson.annotations.SerializedName

data class ApiObjectBlogger(
    @SerializedName("bloger_posts")
    val bloger_posts: MutableList<BlogerPost>,
    @SerializedName("bloger_url")
    val bloger_url: String,
    @SerializedName("success")
    val success: Boolean
)

data class BlogerPost(
    @SerializedName("ID")
    val ID: Int,
    @SerializedName("post_img")
    val post_img: String,
    @SerializedName("post_title")
    val post_title: String,
    @SerializedName("post_url")
    val post_url: String
)