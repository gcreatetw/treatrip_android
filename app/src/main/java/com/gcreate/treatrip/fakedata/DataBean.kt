package com.gcreate.treatrip.fakedata

import androidx.fragment.app.FragmentActivity
import com.gcreate.treatrip.R

class DataBean {

    var imageRes: Int? = null
    var imageUrl: String? = null
    var title: String? = null
    var viewType: Int = 0
    lateinit var cityName_ch: String
    lateinit var cityName_en: String
    var cityID: Int  = 0

    constructor(imageRes: Int? , title: String? , viewType: Int) {
        this.imageRes = imageRes
        this.title = title
        this.viewType = viewType

    }

    constructor(imageUrl: String? , title: String? , viewType: Int) {
        this.imageUrl = imageUrl
        this.title = title
        this.viewType = viewType
    }

    //    constructor(imageUrl: String? , title: String? , viewType: Int) {
    //        this.imageUrl = imageUrl
    //        this.title = title
    //        this.viewType = viewType
    //    }

    constructor(imageRes: Int? , cityName_ch: String , cityName_en: String,cityID:Int) {
        this.imageRes = imageRes
        this.cityName_ch = cityName_ch
        this.cityName_en = cityName_en
        this.cityID = cityID
    }

    companion object {

        //测试数据，如果图片链接失效请更换
        //        val bannerImageWithoutWeb: List<DataBean>
        //            get() {
        //                val list: MutableList<DataBean> = ArrayList()
        //                list.add(DataBean(Global.pdfToBitmap(mContext, )R.mipmap.home_bn01 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn02 , null , 1))
        //                list.add(DataBean(R.mipmap.home_bn03 , null , 1))
        //                return list
        //            }

            val cityList: List<DataBean>
            get() {
                val citylist: MutableList<DataBean> = ArrayList()
                citylist.add(0 , DataBean(R.mipmap.image_cover_taipei02 , "臺北" , "TPE",1792))
                citylist.add(1 , DataBean(R.mipmap.image_cover_outlying_islands , "離島" , "ISLE",2371))
                citylist.add(2 , DataBean(R.mipmap.image_cover_yilan01 , "宜蘭" , "ILA",2367))
                citylist.add(3 , DataBean(R.mipmap.image_cover_taichung , "臺中" , "TXG",30902))
                citylist.add(4 , DataBean(R.mipmap.image_cover_hsinchu01 , "新竹" , "HSZ",2369))
                citylist.add(5 , DataBean(R.mipmap.image_cover_miaoli , "苗栗" , "ZMI",32247))
                citylist.add(6 , DataBean(R.mipmap.image_cover_hualien , "花蓮" , "HUN",2373))
                citylist.add(7 , DataBean(R.mipmap.image_cover_xinbei , "新北" , "NTPC",32428))
                citylist.add(8 , DataBean(R.mipmap.image_cover_taitung01 , "臺東" , "TTT",2372))
                citylist.add(9 , DataBean(R.mipmap.image_cover_keelung , "基隆" , "KEL",32721))
                citylist.add(10 , DataBean(R.mipmap.image_cover_tainan , "臺南" , "TNN",36850))
                citylist.add(11 , DataBean(R.mipmap.image_cover_changhua , "彰化" , "CHW",32887))
                citylist.add(12 , DataBean(R.mipmap.image_cover_taoyuan01 , "桃園" , "TYN",30501))
                citylist.add(13 , DataBean(R.mipmap.image_cover_nantou , "南投" , "NTC",35807))
                citylist.add(14 , DataBean(R.mipmap.image_cover_yunlin , "雲林" , "YUN",36365))
                citylist.add(15 , DataBean(R.mipmap.image_cover_kaohsiung , "高雄" , "KHH",37212))
                citylist.add(16 , DataBean(R.mipmap.image_cover_chiayi , "嘉義" , "CYI",36536))
                citylist.add(17 , DataBean(R.mipmap.image_cover_pingtung , "屏東" , "PIF",37367))

                return citylist
            }

        fun bannerImages(context: FragmentActivity?): List<DataBean> {
            val list: MutableList<DataBean> = ArrayList()
            list.add(DataBean(R.mipmap.home_bn01 , null , 1))
            list.add(DataBean(R.mipmap.home_bn02 , null , 1))
            list.add(DataBean(R.mipmap.home_bn03 , null , 1))
            return list
        }

    }


}
