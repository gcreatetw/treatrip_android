package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.treatrip.R
import com.gcreate.treatrip.fakedata.AttractionInfoModel
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.tools.ExpandableTextView

class AttractionInfoItemAdapter(private val dataSet: List<AttractionInfoModel>) : RecyclerView.Adapter<AttractionInfoItemAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById<View>(R.id.tv_attraction_title) as TextView
        val content: ExpandableTextView = itemView.findViewById<View>(R.id.expand_text_view) as ExpandableTextView
    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_attractions , parent , false)
        view.setBackgroundColor(parent.context.resources.getColor(android.R.color.transparent))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {

        holder.title.text = dataSet[position].articleTitle
        holder.content.text = HtmlCompat.fromHtml(dataSet[position].articleContent , HtmlCompat.FROM_HTML_MODE_LEGACY).toString()

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }


    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }


}