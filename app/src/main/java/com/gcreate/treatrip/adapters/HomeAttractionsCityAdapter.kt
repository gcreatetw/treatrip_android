package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.gcreate.treatrip.R
import com.gcreate.treatrip.fakedata.DataBean
import com.gcreate.treatrip.listener.ItemClickListener

class HomeAttractionsCityAdapter(private val mActivity: FragmentActivity? , private val dataSet: List<DataBean>) : RecyclerView.Adapter<HomeAttractionsCityAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val city_ImageView: ImageView = itemView.findViewById<View>(R.id.img_city) as ImageView
        val cityName_CH: TextView = itemView.findViewById<View>(R.id.cityName_CH) as TextView
        val cityName_EN: TextView = itemView.findViewById<View>(R.id.cityName_EN) as TextView
    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_city , parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        Glide.with(mActivity!!).load(dataSet[position].imageRes).into(holder.city_ImageView)
        holder.cityName_CH.text = dataSet[position].cityName_ch
        holder.cityName_EN.text = dataSet[position].cityName_en

        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(position)
        }

    }

    override fun getItemCount(): Int {
        return dataSet.size
    }


    // 提供set方法
    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}