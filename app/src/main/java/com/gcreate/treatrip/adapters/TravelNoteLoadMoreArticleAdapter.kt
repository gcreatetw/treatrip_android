package com.gcreate.treatrip.adapters

import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.R
import com.gcreate.treatrip.webAPI.Article

class TravelNoteLoadMoreArticleAdapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<Article>) : BaseQuickAdapter<Article , BaseViewHolder>(layoutResId , data) , LoadMoreModule {

    override fun convert(holder: BaseViewHolder , item: Article) {

        val itemImage: ImageView = holder.getView(R.id.item_image)
        Glide.with(mActivity).load(item.post_img).transform(CenterCrop() , RoundedCorners(25)).into(itemImage)
        holder.setText(R.id.item_title , item.post_title)
    }


}
