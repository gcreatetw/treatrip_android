package com.gcreate.treatrip.adapters

import android.graphics.drawable.PictureDrawable
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.R
import com.gcreate.treatrip.tools.glideloadsvg.SvgSoftwareLayerSetter
import com.gcreate.treatrip.webAPI.DiscoverTaiwain


class TaiwanExplore1Adapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<DiscoverTaiwain>) : BaseQuickAdapter<DiscoverTaiwain , BaseViewHolder>(layoutResId , data) {


    override fun convert(holder: BaseViewHolder , item: DiscoverTaiwain) {
        //  人情文化
        holder.setText(R.id.tv_title , item.main_type)

        val itemImage: ImageView = holder.getView(R.id.logo_title)

        Glide.with(mActivity).`as`(PictureDrawable::class.java).listener(SvgSoftwareLayerSetter()).load(item.icon).into(itemImage)


        val rvContent: RecyclerView = holder.getView(R.id.rv_content)
        rvContent.layoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.VERTICAL , false)
        rvContent.adapter = TaiwanExplore2Adapter(mActivity , R.layout.card_item_recommend_places , item.catgories)
    }


}