package com.gcreate.treatrip.adapters

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.view.ActivityWebView
import com.gcreate.treatrip.view.FragmentWebView
import com.gcreate.treatrip.webAPI.BlogerPost

class BloggerAdapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<BlogerPost>) : BaseQuickAdapter<BlogerPost , BaseViewHolder>(layoutResId , data) , LoadMoreModule {

    //  屏幕的宽度(px值）
    private var screenWidth = mActivity.resources.displayMetrics.widthPixels

    //  Item的宽度，或图片的宽度
    var width = screenWidth / 2

    override fun convert(holder: BaseViewHolder , item: BlogerPost) {
        holder.setText(R.id.item_title , item.post_title)

        val itemImage: ImageView = holder.getView(R.id.item_image)
        Glide.with(mActivity).load(item.post_img).override(width , SIZE_ORIGINAL).fitCenter().into(itemImage)

        holder.itemView.setOnClickListener {
            val webView = FragmentWebView()
            val bundle = Bundle()
            bundle.putString("LinkURL",item.post_url)
            webView.arguments = bundle
            MainActivity.replaceFragment(mActivity , webView)
        }

    }

}


