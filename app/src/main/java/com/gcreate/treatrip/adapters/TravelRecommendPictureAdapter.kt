package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.gcreate.treatrip.R
import com.gcreate.treatrip.listener.ItemClickListener

class TravelRecommendPictureAdapter(private val mActivity: FragmentActivity? , private val dataSet: List<String>) : RecyclerView.Adapter<TravelRecommendPictureAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById<View>(R.id.item_selected_img) as ImageView
    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_upload_picture , parent , false)
        view.setBackgroundColor(parent.context.resources.getColor(android.R.color.transparent))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {

        Glide.with(mActivity!!).load(dataSet[position]).transform(CenterCrop() , RoundedCorners(25)).into(holder.image)

        // 点击事件
        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(position)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}