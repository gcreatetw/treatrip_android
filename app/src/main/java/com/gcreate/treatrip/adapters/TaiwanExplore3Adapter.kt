package com.gcreate.treatrip.adapters

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.R
import com.gcreate.treatrip.view.FragmentAttractionInfo
import com.gcreate.treatrip.webAPI.CatPost


class TaiwanExplore3Adapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<CatPost>) : BaseQuickAdapter<CatPost , BaseViewHolder>(layoutResId , data) {


    override fun convert(holder: BaseViewHolder , item: CatPost) {

        val itemImage: ImageView = holder.getView(R.id.item_image)
        Glide.with(mActivity).load(item.post_img).error(R.mipmap.image_cover_pv).into(itemImage)

        val title: TextView = holder.getView(R.id.item_title)
        title.textSize = 14f
        title.setPadding(0,0,0,4)
        title.text = item.post_title


        holder.itemView.setOnClickListener {
            replaceFragment(mActivity , FragmentAttractionInfo() , item.ID)
        }
    }

    private fun replaceFragment(mainActivity: FragmentActivity , fragment: Fragment , post_id: Int) {
        val fragmentManager = mainActivity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val bundle = Bundle()
        bundle.putString("postID" , post_id.toString())
        fragment.arguments = bundle

        fragmentTransaction.replace(R.id.container , fragment , "")
        fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
    }

}