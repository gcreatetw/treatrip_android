package com.gcreate.treatrip.adapters

import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.R
import com.gcreate.treatrip.webAPI.Catgory

class TaiwanExplore2Adapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<Catgory>) : BaseQuickAdapter<Catgory , BaseViewHolder>(layoutResId , data) {


    override fun convert(holder: BaseViewHolder , item: Catgory) {
        //  文化 CULTURE
        val title: TextView = holder.getView(R.id.tv_title)
        title.textSize = 16f
        title.text = item.cat_title

        val rvContent: RecyclerView = holder.getView(R.id.rv_content)
        rvContent.layoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.HORIZONTAL , false)
        rvContent.adapter = TaiwanExplore3Adapter(mActivity , R.layout.card_item_recommend_subitem , item.cat_posts)

    }


}