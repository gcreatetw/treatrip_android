package com.gcreate.treatrip.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.gcreate.treatrip.R
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.view.FragmentAttractionInfo
import com.gcreate.treatrip.webAPI.RecommendAttraction

class CityRecommend2Adapter(private val mActivity: FragmentActivity? , private val dataSet: List<RecommendAttraction>) : RecyclerView.Adapter<CityRecommend2Adapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById<View>(R.id.item_image) as ImageView
        val title: TextView = itemView.findViewById<View>(R.id.item_title) as TextView

    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_recommend_subitem , parent , false)
        view.setBackgroundColor(parent.context.resources.getColor(android.R.color.transparent))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        Glide.with(mActivity!!).load(dataSet[position].post_img).error(R.mipmap.image_post_default).transform(CenterCrop() , RoundedCorners(25)).into(holder.image)
        holder.title.text = dataSet[position].post_title

        holder.itemView.setOnClickListener {
            replaceFragment(mActivity , FragmentAttractionInfo() , dataSet[position].ID)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }


    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    private fun replaceFragment(mainActivity: FragmentActivity , fragment: Fragment , post_id: Int) {
        val fragmentManager = mainActivity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val bundle = Bundle()
        bundle.putString("postID" , post_id.toString())
        fragment.arguments = bundle

        fragmentTransaction.replace(R.id.container , fragment , "")
        fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
    }

}