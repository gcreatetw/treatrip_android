package com.gcreate.treatrip.adapters

import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.gcreate.treatrip.R
import com.gcreate.treatrip.webAPI.Adventure


class ThemeAdventureAdapter(private val mActivity: FragmentActivity , layoutResId: Int , data: MutableList<Adventure>) : BaseQuickAdapter<Adventure , BaseViewHolder>(layoutResId , data) , LoadMoreModule {

    //屏幕的宽度(px值）
    var screenWidth = mActivity.resources.displayMetrics.widthPixels

    //Item的宽度，或图片的宽度
    var width = screenWidth / 2

    override fun convert(holder: BaseViewHolder , item: Adventure) {
        holder.setText(R.id.item_title , item.post_title)
        val itemImage: ImageView = holder.getView(R.id.item_image)

        Glide.with(mActivity).load(item.post_img)
            .override(width,SIZE_ORIGINAL)
            .fitCenter()
            .into(itemImage)
    }

}


