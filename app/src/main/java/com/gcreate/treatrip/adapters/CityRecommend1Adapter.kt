package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.treatrip.R
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.webAPI.Town

class CityRecommend1Adapter(val mActivity: FragmentActivity? , val mRecyclerView: RecyclerView? , private val dataSet: List<Town>) : RecyclerView.Adapter<CityRecommend1Adapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null
    private val VIEW_TYPE_ITEM = 1
    private val VIEW_TYPE_FOOTER = 2

    var parentHeight: Int? = 0
    var itemHeight: Int? = 0
    var footerView: View? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var township: TextView? = null
        var rvContent: RecyclerView? = null

        init {
            when (itemView) {
                footerView -> {

                }
                else -> {
                    township = itemView.findViewById<View>(R.id.tv_title) as TextView
                    rvContent = itemView.findViewById<View>(R.id.rv_content) as RecyclerView
                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        return if (viewType == VIEW_TYPE_ITEM) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_recommend_places , parent , false)
            view.post {
                parentHeight = mRecyclerView?.height
                itemHeight = view.height
            }
            ViewHolder(view)
        } else {
            //            footerView = LayoutInflater.from(parent.context).inflate(R.layout.card_item_upload_picture , parent , false)
            footerView = View(parent.context)
            footerView!!.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT , parentHeight!! - itemHeight!!)
            ViewHolder(footerView!!)
        }


    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        //        Glide.with(mActivity!!).load(dataSet[position].imageRes).into(holder.city_ImageView)
        if (position != dataSet.size) {
            holder.township!!.text = dataSet[position].township
            // Sub recyclerview content
            val subItemAdapter = CityRecommend2Adapter(mActivity , dataSet[position].Recommend_Attractions)
            holder.rvContent!!.adapter = subItemAdapter
            holder.rvContent!!.layoutManager = LinearLayoutManager(mActivity , LinearLayoutManager.HORIZONTAL , false)

        }

    }

    override fun getItemCount(): Int {
        return dataSet.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == dataSet.size) {
            VIEW_TYPE_FOOTER
        } else {
            VIEW_TYPE_ITEM
        }

    }

    // 提供set方法
    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}