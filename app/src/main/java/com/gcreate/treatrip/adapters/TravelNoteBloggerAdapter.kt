package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.gcreate.treatrip.R
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.webAPI.Blogger

class TravelNoteBloggerAdapter(private val mActivity: FragmentActivity? , private val dataSet: List<Blogger>) : RecyclerView.Adapter<TravelNoteBloggerAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById<View>(R.id.item_blogger_image) as ImageView
        val title: TextView = itemView.findViewById<View>(R.id.item_blogger_title) as TextView
        val cats: TextView = itemView.findViewById<View>(R.id.item_blogger_cats) as TextView

    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_blogger , parent , false)
        view.setBackgroundColor(parent.context.resources.getColor(android.R.color.transparent))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        Glide.with(mActivity!!).load(dataSet[position].post_img).error(R.drawable.icon_blogger).apply(RequestOptions.circleCropTransform()).into(holder.image)

        holder.title.text = dataSet[position].post_title
        holder.cats.text = dataSet[position].blogger_job_title

        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(position)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}