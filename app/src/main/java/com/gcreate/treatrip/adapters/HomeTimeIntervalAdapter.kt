package com.gcreate.treatrip.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.gcreate.treatrip.R
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.webAPI.LimitedTime

class HomeTimeIntervalAdapter(private val mActivity: FragmentActivity? , private val dataSet: List<LimitedTime>) : RecyclerView.Adapter<HomeTimeIntervalAdapter.ViewHolder>() {

    private var itemClickListener: ItemClickListener? = null

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById<View>(R.id.item_image) as ImageView
        val title: TextView = itemView.findViewById<View>(R.id.item_title) as TextView

    }

    override fun onCreateViewHolder(parent: ViewGroup , viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.card_item_time_interval , parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder , position: Int) {
        Glide.with(mActivity!!).load(dataSet[position].post_img).error(R.mipmap.image_post_default).transform(CenterCrop() , RoundedCorners(25)).into(holder.image)

        holder.title.text = dataSet[position].post_title

        // 点击事件
        holder.itemView.setOnClickListener {
            itemClickListener!!.onItemClickListener(position)
        }
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    // 提供set方法
    fun setOnKotlinItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }


}