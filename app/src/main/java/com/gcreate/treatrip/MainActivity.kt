package com.gcreate.treatrip

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import com.gcreate.treatrip.databinding.ActivityMainBinding
import com.gcreate.treatrip.view.FragmentAttractions
import com.gcreate.treatrip.view.FragmentGoWhere
import com.gcreate.treatrip.view.FragmentRecommend
import com.gcreate.treatrip.view.FragmentTravelNote

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var navigationSelectedPosition = 0

    override fun onStart() {
        super.onStart()

        if (intent!!.getBooleanExtra("isComeFromWebActivity" , false)) {
            replaceFragment(this , FragmentGoWhere())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this , R.layout.activity_main)

        initView()

        binding.bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.item_attractions -> {
                    if (!currentFragmentName.equals("FragmentAttractions")) replaceFragment(this , FragmentAttractions())
                    navigationSelectedPosition = 0
                }
                R.id.item_travel_notes -> {
                    if (!currentFragmentName.equals("FragmentTravelNote")) replaceFragment(this , FragmentTravelNote())
                    navigationSelectedPosition = 1
                }
                R.id.item_recommend -> {
                    if (!currentFragmentName.equals("FragmentRecommend")) replaceFragment(this , FragmentRecommend())
                    navigationSelectedPosition = 2
                }
            }
            true
        }
    }

    private fun initView() {
        replaceFragment(this , FragmentAttractions())
    }

    override fun onBackPressed() {
        val fragmentManager = supportFragmentManager
        if (fragmentManager.backStackEntryCount > 1) {
            fragmentManager.popBackStackImmediate()
            val fragment = supportFragmentManager.findFragmentById(R.id.container)
            // the fragment exist
            when {
                fragment.toString().startsWith("FragmentAttractions") -> {
                    binding.bottomNavigationView.menu.findItem(R.id.item_attractions).isChecked = true
                    currentFragmentName = "FragmentAttractions"
                }
                fragment.toString().startsWith("FragmentTravelNote") -> {
                    binding.bottomNavigationView.menu.findItem(R.id.item_travel_notes).isChecked = true
                    currentFragmentName = "FragmentTravelNote"
                }
                fragment.toString().startsWith("FragmentRecommend") -> {
                    binding.bottomNavigationView.menu.findItem(R.id.item_recommend).isChecked = true
                    currentFragmentName = "FragmentRecommend"
                }
            }
        } else {
            finish()
        }
    }

    // 取得當前 MainActivity fragment container 是哪個fragment
    companion object {
        var currentFragmentName: String? = null

        fun replaceFragment(mainActivity: FragmentActivity? , fragment: Fragment) {
            val fragmentManager = mainActivity?.supportFragmentManager
            val fragmentTransaction = fragmentManager?.beginTransaction()
            fragmentTransaction?.replace(R.id.container , fragment , "")
            fragmentTransaction?.addToBackStack(fragment.toString())
            fragmentTransaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            fragmentTransaction?.commit()
            mainActivity?.supportFragmentManager!!.executePendingTransactions()
            getCurrentFragmentName(mainActivity.supportFragmentManager.findFragmentById(R.id.container)!!)
        }

        private fun getCurrentFragmentName(fragment: Fragment) {
            val strings = fragment.toString().split("\\{".toRegex()).toTypedArray()
            currentFragmentName = strings[0]

        }
    }
}