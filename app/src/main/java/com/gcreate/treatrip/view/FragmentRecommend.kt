package com.gcreate.treatrip.view

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.treatrip.BuildConfig
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.TravelRecommendPictureAdapter
import com.gcreate.treatrip.databinding.FragmentRecommendBinding
import com.gcreate.treatrip.listener.CustomClickListener
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectRecommendUploadResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream


/**推薦*/
class FragmentRecommend : Fragment() {

    private lateinit var binding: FragmentRecommendBinding
    private lateinit var imageAdapter: TravelRecommendPictureAdapter

    private val SELECT_IMAGE = 100
    private var clickPosition = 0
    private val imageRealPathArray: ArrayList<String> = ArrayList()
    private val imageBase64StringArray: ArrayList<String> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_recommend , container , false)

        initViewPosition()
        setRecommendSelectPictureView()

        //  想趣哪裡
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

        binding.sendBtn.setOnClickListener(object : CustomClickListener() {
            override fun onNoDoubleClick(v: View?) {
                sendAttractionInfoToServer()
            }
        })

        return binding.root
    }

    private fun initViewPosition() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()

        binding.search.iconArrow.visibility = View.GONE

        val set = ConstraintSet()
        set.clone(binding.clTop)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , marginTopEdge)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.clTop)
    }

    private fun setRecommendSelectPictureView() {

        //  從相簿選圖片
        val intent = Intent(Intent.ACTION_PICK)
        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI , "image/*")

        //  預設放 5張圖的位置
        for (i: Int in 0..4) {
            imageRealPathArray.add(i , "")
        }

        imageAdapter = TravelRecommendPictureAdapter(activity , imageRealPathArray)
        binding.rvRecommendPictures.adapter = imageAdapter
        binding.rvRecommendPictures.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)

        imageAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                clickPosition = position
                //  檢查存取權限
                if (checkPermission()) {
                    startActivityForResult(intent , SELECT_IMAGE)
                } else {
                    Toast.makeText(activity , "請先開啟存取權限" , Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    /* 選取照片後回調
          *  reference  article https://blog.csdn.net/qq_33275597/article/details/52081008
        */
    override fun onActivityResult(requestCode: Int , resultCode: Int , data: Intent?) {
        super.onActivityResult(requestCode , resultCode , data)

        if (resultCode == Activity.RESULT_OK) {
            if (BuildConfig.DEBUG && data == null) {
                error("Assertion failed")
            }
            when (requestCode) {
                SELECT_IMAGE -> {
                    //  獲取返回的數據，這裡是 android自定義的 Uri 地址
                    val selectedImage: Uri? = data!!.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)
                    //  獲取選擇照片的數據圖片
                    val cursor = selectedImage?.let { activity!!.contentResolver.query(it , filePathColumn , null , null , null) }
                    cursor!!.moveToFirst()
                    //  從選中圖片中獲取圖片的路徑
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    val picturePath = cursor.getString(columnIndex)
                    cursor.close()
                    //  將圖片顯示到界面上
                    imageRealPathArray.removeAt(clickPosition)
                    imageRealPathArray.add(clickPosition , picturePath)
                    imageAdapter.notifyDataSetChanged()

                    //  ImageRealPath  to Bitmap and convert to Base64:
                    imageBase64StringArray.add(encode(picturePath))
                }
            }
        }
    }

    private fun checkPermission(): Boolean {
        val hasGone2: Int = ActivityCompat.checkSelfPermission(context!! , Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return hasGone2 == PackageManager.PERMISSION_GRANTED
    }

    private fun encode(path: String): String {
        //  decode to bitmap
        val bitmap = BitmapFactory.decodeFile(path)

        //  convert to byte array
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG , 100 , baos)
        val bytes = baos.toByteArray()

        //  base64 encode
        val encode = Base64.encode(bytes , Base64.DEFAULT)
        val encodeString = String(encode)

        return encodeString
    }

    private fun sendAttractionInfoToServer() {

        val parm = JSONObject()
        //  景點名稱
        parm.put("attraction_name" , binding.editAttractionName.text!!.toString())
        //  景點地址
        parm.put("address" , binding.editAttractionAddress.text!!.toString())
        //  景點聯絡資訊
        parm.put("contact_info" , binding.editAttractionPhone.text?.toString())
        //  開放時間
        parm.put("banking_hours" , binding.editAttractionOpentime.text?.toString())
        //  景點網址
        parm.put("attraction_url" , binding.editAttractionWeburl.text?.toString())
        //  收費方式
        parm.put("payment_method" , binding.editAttractionPayment.text?.toString())
        //  交通資訊
        parm.put("traffic_info" , binding.editAttractionTrafficway.text?.toString())
        //  照片Base64 String Arrays
        val arr = JSONArray()
        for (s: String in imageBase64StringArray) {
            arr.put(s)
        }
        parm.put("image_array" , arr)
        //  備註
        parm.put("remarks" , binding.editAttractionRecommendAdvise.text.toString())

        val requestBody = RequestBody.create(MediaType.parse("application/json") , parm.toString())

        Global.treatripAPI.getRecommendUploadResult(requestBody).enqueue(object : Callback<ApiObjectRecommendUploadResult> {
            override fun onResponse(call: Call<ApiObjectRecommendUploadResult> , response: Response<ApiObjectRecommendUploadResult>) {
                val objectRecommendUploadResult = response.body()
                Log.d("Ben" , objectRecommendUploadResult!!.message)
                val alertDialog = MaterialAlertDialogBuilder(context!!)
                alertDialog.setMessage(objectRecommendUploadResult.message)
                alertDialog.setPositiveButton("OK" , DialogInterface.OnClickListener { dialog , which ->
                    resetInfo()
                    dialog.dismiss()
                })
                alertDialog.setCancelable(false)
                alertDialog.show()
            }

            override fun onFailure(call: Call<ApiObjectRecommendUploadResult> , t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }

    private fun resetInfo(){
        binding.editAttractionName.setText("")
        binding.editAttractionAddress.setText("")
        binding.editAttractionPhone.setText("")
        binding.editAttractionOpentime.setText("")
        binding.editAttractionWeburl.setText("")
        binding.editAttractionPayment.setText("")
        binding.editAttractionTrafficway.setText("")
        for (i: Int in 0..4) {
            imageRealPathArray[i] = ""
        }
        imageBase64StringArray.removeAll(imageBase64StringArray)
        imageAdapter.notifyDataSetChanged()
        binding.editAttractionRecommendAdvise.setText("")
    }

}