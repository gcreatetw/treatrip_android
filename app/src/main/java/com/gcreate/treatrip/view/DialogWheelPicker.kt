package com.gcreate.treatrip.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.aigestudio.wheelpicker.WheelPicker
import com.gcreate.treatrip.R
import com.gcreate.treatrip.databinding.DialogWheelPickerBinding
import com.gcreate.treatrip.listener.UpdateCityNameListenerManager
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.Global


class DialogWheelPicker : DialogFragment() {

    lateinit var binding: DialogWheelPickerBinding
    private val city: String by Arg("cityName" , "")
    private var stopPosition : Int ? =0

    companion object {
        val cityList = listOf<String>("基隆市" , "台北市" , "新北市" , "桃園市" , "新竹縣" ,
            "新竹市" , "苗栗縣" , "臺中市" , "彰化縣" , "南投縣" , "雲林縣" , "嘉義縣" ,
            "嘉義市" , "台南市" , "高雄市" , "屏東縣" , "宜蘭縣" , "台東縣" , "花蓮縣" , "澎湖縣")
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setLayout((Global.windowWidth * 0.8).toInt() , (Global.windowHeight * 0.6).toInt())
            getDialog()!!.window!!.setBackgroundDrawableResource(R.drawable.bg_solid_r30_white_ffffff)
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater , R.layout.dialog_wheel_picker , container , false)

        viewAction()

        binding.numberPicker.data = cityList
        binding.numberPicker.setSelectedItemPosition(currentPosition(),false)
        binding.numberPicker.setOnWheelChangeListener(object : WheelPicker.OnWheelChangeListener{
            override fun onWheelScrolled(offset: Int) {
            }

            override fun onWheelSelected(position: Int) {
                stopPosition = position
            }

            override fun onWheelScrollStateChanged(state: Int) {

            }

        })



        return binding.root
    }


    private fun viewAction() {
        binding.imgCloseNP.setOnClickListener {
            dialog!!.dismiss()
        }

        binding.btnInputNP.setOnClickListener {
            UpdateCityNameListenerManager.getInstance().sendBroadCast(cityList[stopPosition!!])
            dialog!!.dismiss()
        }
    }

    private fun currentPosition(): Int {
        var selectPosition = 0
        for (i in cityList.indices) {
            if (cityList[i] == city)
                selectPosition = i
        }
        return selectPosition
    }

}