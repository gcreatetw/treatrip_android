package com.gcreate.treatrip.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.ThemeAdventureAdapter
import com.gcreate.treatrip.databinding.FragmentThemeAdventureBinding
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.Adventure
import com.gcreate.treatrip.webAPI.ApiObjectThemeAdventure
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentThemeAdventure : Fragment() {

    //  主題冒險
    private lateinit var binding: FragmentThemeAdventureBinding
    private lateinit var objectThemeAdventure: ApiObjectThemeAdventure

    var currentPage: Int = 1
    private lateinit var mAdapter: ThemeAdventureAdapter
    private var rowsArrayList: ArrayList<Adventure> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_theme_adventure , container , false)

        initView()
        callThemeAdventureAPI(currentPage)


        return binding.root
    }


    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }


        // recycler view Data binding
        val layoutManager = StaggeredGridLayoutManager(2 , StaggeredGridLayoutManager.VERTICAL)
        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE    //  防止瀑布流 item交换位置
        binding.rvContent.layoutManager = layoutManager

        mAdapter = ThemeAdventureAdapter(activity!! , R.layout.card_item_theme_adventure , rowsArrayList)
        binding.rvContent.adapter = mAdapter

        // 获取模块
        mAdapter.loadMoreModule
        // 打开或关闭加载更多功能（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMore = true
        // 是否自定加载下一页（默认为true）
        mAdapter.loadMoreModule.isAutoLoadMore = true
        // 当数据不满一页时，是否继续自动加载（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false
        // 所有数据加载完成后，是否允许点击（默认为false）
        mAdapter.loadMoreModule.enableLoadMoreEndClick = false
        // 是否处于加载中
        mAdapter.loadMoreModule.isLoading

        mAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(adapter: BaseQuickAdapter<* , *> , view: View , position: Int) {
                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL",rowsArrayList[position].post_url)
                webView.arguments = bundle
                MainActivity.replaceFragment(activity , webView)
            }
        })
    }

    private fun callThemeAdventureAPI(Page: Int) {

        val params = JSONObject()
        params.put("page" , Page)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , params.toString())

        Global.treatripAPI.getThemeAdventure(requestBody).enqueue(object : Callback<ApiObjectThemeAdventure> {

            override fun onResponse(call: Call<ApiObjectThemeAdventure> , response: Response<ApiObjectThemeAdventure>) {
                objectThemeAdventure = response.body()!!

                if (objectThemeAdventure.adventure.size < 20) {
                    currentPage--
                    /******************************** 状态设置 ********************************/
                    // 当前这次数据加载完毕，调用此方法
                    mAdapter.loadMoreModule.loadMoreComplete()
                    // 所有数据加载完成，调用此方法
                    // 需要重置"加载完成"状态时，请调用 setNewData()
                    mAdapter.loadMoreModule.loadMoreEnd()

                }

                populateData(objectThemeAdventure)
            }

            override fun onFailure(call: Call<ApiObjectThemeAdventure> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })
    }


    private fun populateData(objectThemeAdventure: ApiObjectThemeAdventure) {

        for (i in 0 until objectThemeAdventure.adventure.size) {
            rowsArrayList.add(objectThemeAdventure.adventure[i])
        }

        mAdapter.setNewInstance(rowsArrayList)
        mAdapter.notifyItemRangeChanged(rowsArrayList.size - objectThemeAdventure.adventure.size , rowsArrayList.size - 1)
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false

        mAdapter.loadMoreModule.setOnLoadMoreListener {
            currentPage++
            callThemeAdventureAPI(currentPage)
        }

        // 预加载的位置（默认为1）
        mAdapter.loadMoreModule.preLoadNumber = currentPage

    }

}



