package com.gcreate.treatrip.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.listener.InterruptCallControlListener
import com.gcreate.treatrip.listener.InterruptCallControlListenerManager
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.AttractionInfoImageAdapter
import com.gcreate.treatrip.adapters.AttractionInfoItemAdapter
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.databinding.FragmentAttractionInfoBinding
import com.gcreate.treatrip.fakedata.AttractionInfoModel
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectAttractionInfo
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentAttractionInfo : Fragment() , InterruptCallControlListener {


    private lateinit var binding: FragmentAttractionInfoBinding
    private lateinit var loadingDialog: DialogLoading
    private lateinit var call: Call<ApiObjectAttractionInfo>

    companion object {
        var objectAttractionInfo: ApiObjectAttractionInfo? = null
    }

    private val postID: String by Arg("postID" , "")

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_attraction_info , container , false)
        InterruptCallControlListenerManager.getInstance()?.registerListener(this)
        initView()

        // Loafing Animate
        loadingDialog = DialogLoading()
        loadingDialog.show(activity!!.supportFragmentManager , "simple dialog")

        processThread()


        return binding.root
    }

    private fun processThread() {
        //构建一个下载进度条
        object : Thread() {
            override fun run() {
                //在新线程里执行长耗时方法
                getServerDate(postID)
            }
        }.start()
    }

    private val handler: Handler = @SuppressLint("HandlerLeak") object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            //只要执行到这里就关闭对话框
            loadingDialog.dismissDialog()
        }
    }

    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }

        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

    }

    private fun getServerDate(postID: String) {
        val parm = JSONObject()
        parm.put("post_id" , postID)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , parm.toString())

        call = Global.treatripAPI.getAttractionInfo(requestBody)
        call.enqueue(object : Callback<ApiObjectAttractionInfo> {
            override fun onResponse(call: Call<ApiObjectAttractionInfo> , response: Response<ApiObjectAttractionInfo>) {
                objectAttractionInfo = response.body()!!
                bindDataToView(objectAttractionInfo!!)
                //执行完毕后给handler发送一个空消息關閉 Loading Dialog
                handler.sendEmptyMessage(0)
                binding.lyContent.visibility = View.VISIBLE
            }

            override fun onFailure(call: Call<ApiObjectAttractionInfo> , t: Throwable) {
                Log.d("API request error" , t.message.toString())
            }

        })

    }

    private fun bindDataToView(objectAttractionInfo: ApiObjectAttractionInfo?) {

        val postInfo = objectAttractionInfo!!.post_info

        if (postInfo.post_img.isNotEmpty()) {
            Glide.with(activity!!).load(postInfo.post_img[0].picture).error(R.mipmap.home_bn03).into(binding.mImgView)
        } else {
            Glide.with(activity!!).load(R.mipmap.home_bn03).into(binding.mImgView)
        }

        binding.tvAttractionName.text = postInfo.post_title

        //  電話
        if (postInfo.tel.isNullOrEmpty()) binding.lyAttractionPhone.visibility = GONE
        else binding.tvAttractionPhone.text = postInfo.tel

        //  地址
        if (postInfo.add.isNullOrEmpty()) binding.lyAttractionAddress.visibility = GONE
        else binding.tvAttractionAddress.text = postInfo.add

        //  經緯度
        if (postInfo.px.isNullOrEmpty() || postInfo.py.isNullOrEmpty()) binding.lyAttractionLatitudeLongitude.visibility = GONE
        else {
            val latitudeLongitude = postInfo.px + "/" + postInfo.py
            binding.tvAttractionLatitudeLongitude.text = latitudeLongitude
        }
        //  網址
        if (postInfo.website.isNullOrEmpty()) binding.lyAttractionWebsite.visibility = GONE
        else binding.tvAttractionWebsite.text = postInfo.website

        // TAG 標籤
        val tagList = arrayListOf<String>()
        if (postInfo.cat.isNotEmpty()) {
            for (i in postInfo.cat.indices) {
                tagList.add(i , postInfo.cat[i])
            }
        }

        if (postInfo.tag.isNotEmpty()) {
            for (i in postInfo.tag.indices) {
                tagList.add(postInfo.tag[i])
            }
        }
        binding.tagContainer.tags = tagList

        // 景點圖片
        val imageAdapter = AttractionInfoImageAdapter(activity , postInfo.post_img)
        binding.attractionImages.componentRv.adapter = imageAdapter
        binding.attractionImages.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)

        imageAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                Toast.makeText(activity , "click " + position + "image" , Toast.LENGTH_SHORT).show()
            }
        })

        //  單一景點的 景點資訊、交通資訊、開放時間
        val attractionInfoDataList = arrayListOf<AttractionInfoModel>()
        //  景點資訊
        if (!postInfo.post_content.isNullOrEmpty()) {
            attractionInfoDataList.add(AttractionInfoModel("景點資訊" , postInfo.post_content))
        }
        //  交通資訊
        if (!postInfo.travellinginfo.isNullOrEmpty()) {
            attractionInfoDataList.add(AttractionInfoModel("交通資訊" , postInfo.travellinginfo))
        }
        //  開放時間
        if (!postInfo.opentime.isNullOrEmpty()) {
            attractionInfoDataList.add(AttractionInfoModel("開放時間" , postInfo.opentime))
        }

        binding.attractionInfos.componentRv.adapter = AttractionInfoItemAdapter(attractionInfoDataList)
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins(0 , 34 , 0 , 0)
        binding.attractionInfos.componentRv.layoutParams = params
        binding.attractionInfos.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.VERTICAL , false)

        imageAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                Toast.makeText(activity , "click " + position + "image" , Toast.LENGTH_SHORT).show()
            }
        })


    }

    override fun notifyViewCancelCall() {
        call.cancel()
        activity!!.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        InterruptCallControlListenerManager.getInstance()!!.unRegisterListener(this)
    }

}


