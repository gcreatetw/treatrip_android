package com.gcreate.treatrip.view

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.GoWhereLocalAdapter
import com.gcreate.treatrip.adapters.GoWhereNearAdapter
import com.gcreate.treatrip.adapters.ItemPagerAdapter
import com.gcreate.treatrip.databinding.FragmentGoWhereBinding
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.listener.UpdateCityNameListenerManager
import com.gcreate.treatrip.listener.UpdateNameListener
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.tools.bottomsheetbehavior.BottomSheetBehaviorGoogleMapsLike
import com.gcreate.treatrip.view.FragmentAttractions.Companion.bannerImgList
import com.gcreate.treatrip.webAPI.ApiLoadCompleteCallback
import com.gcreate.treatrip.webAPI.ApiObjectGoWhere
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/*  想趣哪裡
* */
class FragmentGoWhere : Fragment() , ApiLoadCompleteCallback , UpdateNameListener , TextView.OnEditorActionListener {

    lateinit var binding: FragmentGoWhereBinding
    private lateinit var objectGoWhere: ApiObjectGoWhere
//    private val city: String by Arg("userLocationCity" , "")


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_go_where , container , false)
        UpdateCityNameListenerManager.getInstance().registerListener(this)
        binding.searchEditTxt.setOnEditorActionListener(this)


        viewSetting()
        callGoWhereAPI(FragmentAttractions.userLocationCity, "")



        /**
         * If we want to listen for states callback
         */
        binding.bottomSheet.isNestedScrollingEnabled = false
        val behavior: BottomSheetBehaviorGoogleMapsLike<*> = BottomSheetBehaviorGoogleMapsLike.from(binding.bottomSheet)
        behavior.addBottomSheetCallback(object : BottomSheetBehaviorGoogleMapsLike.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View , newState: Int) {
                when (newState) {
                    BottomSheetBehaviorGoogleMapsLike.STATE_COLLAPSED -> Log.d("bottomsheet-" , "STATE_COLLAPSED")
                    BottomSheetBehaviorGoogleMapsLike.STATE_DRAGGING -> Log.d("bottomsheet-" , "STATE_DRAGGING")
                    BottomSheetBehaviorGoogleMapsLike.STATE_EXPANDED -> Log.d("bottomsheet-" , "STATE_EXPANDED")
                    BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT -> Log.d("bottomsheet-" , "STATE_ANCHOR_POINT")
                    BottomSheetBehaviorGoogleMapsLike.STATE_HIDDEN -> Log.d("bottomsheet-" , "STATE_HIDDEN")
                    else -> Log.d("bottomsheet-" , "STATE_SETTLING")
                }
            }

            override fun onSlide(bottomSheet: View , slideOffset: Float) {}
        })

        //        MergedAppBarLayout mergedAppBarLayout = findViewById(R.id.mergedappbarlayout);
        //        MergedAppBarLayoutBehavior mergedAppBarLayoutBehavior = MergedAppBarLayoutBehavior.from(mergedAppBarLayout);
        //        mergedAppBarLayoutBehavior.setToolbarTitle("Title Dummy");
        //        mergedAppBarLayoutBehavior.setNavigationOnClickListener(new View.OnClickListener() {
        //            @Override
        //            public void onClick(View v) {
        //                behavior.setState(BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT);
        //            }
        //        });

        val adapter = ItemPagerAdapter(activity , bannerImgList)
        binding.pager.adapter = adapter
        behavior.state = BottomSheetBehaviorGoogleMapsLike.STATE_ANCHOR_POINT
        //behavior.setCollapsible(false);
        return binding.root
    }

    private fun viewSetting() {
        Log.d("Ben","66666")
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)
        binding.toolbar.title = ""
        binding.toolbar.setNavigationIcon(R.drawable.icon_collapse)
        binding.toolbar.navigationIcon!!.setColorFilter(resources.getColor(R.color.white) , PorterDuff.Mode.SRC_ATOP)

        binding.toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()
        }

        binding.cityname.text = FragmentAttractions.userLocationCity
        binding.searchEditTxt.hint = "搜尋" + binding.cityname.text + "附近景點"

        binding.lyCity.setOnClickListener {
            binding.searchEditTxt.clearFocus()
            val dialogFragment: DialogFragment = DialogWheelPicker()
            // Supply num input as an argument.
            // Supply num input as an argument.
            val args = Bundle()
            args.putString("cityName" , binding.cityname.text.toString())
            dialogFragment.setArguments(args)
            dialogFragment.show((context as AppCompatActivity).supportFragmentManager , "simple dialog")
        }
    }

    private fun callGoWhereAPI(city: String , keyword: String) {

        val params = JSONObject()
        params.put("city" , city)
        params.put("keyword" , keyword)

        val requestBody = RequestBody.create(MediaType.parse("application/json") , params.toString())
        Global.treatripAPI.getGoWhereResult(requestBody).enqueue(object : Callback<ApiObjectGoWhere> {

            override fun onResponse(call: Call<ApiObjectGoWhere> , response: Response<ApiObjectGoWhere>) {
                objectGoWhere = response.body()!!
                loadComplete()
            }

            override fun onFailure(call: Call<ApiObjectGoWhere> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })

    }

    override fun loadComplete() {
        setLocalAttractionRecyclerView()
        setNearAttractionRecyclerView()
    }

    // 當地遊記
    private fun setLocalAttractionRecyclerView() {
        binding.bottomSheetContent.rvLocalAttraction.imgTitle.setImageResource(R.drawable.icon_travel_recommend)
        binding.bottomSheetContent.rvLocalAttraction.tvTitle.text = "當地遊記"
        binding.bottomSheetContent.rvLocalAttraction.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)

        val localAdapter = GoWhereLocalAdapter(activity , objectGoWhere.posts)
        binding.bottomSheetContent.rvLocalAttraction.componentRv.adapter = localAdapter
        localAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL",objectGoWhere.posts[position].post_url)
                webView.arguments = bundle
                MainActivity.replaceFragment(activity , webView)
            }
        })

    }

    // 附近景點
    private fun setNearAttractionRecyclerView() {
        binding.bottomSheetContent.rvNearAttraction.imgTitle.setImageResource(R.drawable.icon_near_attraction)
        binding.bottomSheetContent.rvNearAttraction.tvTitle.text = "附近景點"
        binding.bottomSheetContent.rvNearAttraction.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.VERTICAL , false)

        val nearAttractionAdapter = GoWhereNearAdapter(activity , objectGoWhere.city_posts)
        binding.bottomSheetContent.rvNearAttraction.componentRv.adapter = nearAttractionAdapter
        nearAttractionAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                replaceFragment(activity!! , FragmentAttractionInfo() , objectGoWhere.city_posts[position].ID)
            }
        })
    }

    private fun replaceFragment(mainActivity: FragmentActivity , fragment: Fragment , post_id: Int) {
        val fragmentManager = mainActivity.supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()

        val bundle = Bundle()
        bundle.putString("postID" , post_id.toString())
        fragment.arguments = bundle

        fragmentTransaction.replace(R.id.container , fragment , "")
        fragmentTransaction.addToBackStack(fragment.toString())
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction.commit()
    }

    override fun notifyUserName(cityName: String) {
        binding.cityname.text = cityName
        binding.searchEditTxt.hint = "搜尋" + cityName + "附近景點"
        callGoWhereAPI(cityName , "")
    }


    override fun onStop() {
        super.onStop()
        binding.searchEditTxt.clearFocus()
    }

    override fun onDestroy() {
        super.onDestroy()
        UpdateCityNameListenerManager.getInstance().unRegisterListener(this)
    }

    override fun onEditorAction(v: TextView? , actionId: Int , event: KeyEvent?): Boolean {
        Log.d("Ben","actionId = " +actionId)
        if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
            hideSoftKeyBroad()
            binding.searchEditTxt.clearFocus()
            callGoWhereAPI(binding.cityname.text.toString() , binding.searchEditTxt.text.toString())
        }
        return true
    }

    private fun hideSoftKeyBroad() {
        val imm = (Objects.requireNonNull(activity)!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.root.windowToken , 0)
    }



}