package com.gcreate.treatrip.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.databinding.FragmentWebViewBinding
import com.gcreate.treatrip.listener.WebLoadingListener
import com.gcreate.treatrip.listener.WebLoadingListenerManager
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.Global


class FragmentWebView : Fragment() , WebLoadingListener {

    lateinit var binding: FragmentWebViewBinding
    private var loadingDialog: DialogLoading? = null
    private val url: String by Arg("LinkURL" , "")


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_web_view , container , false)

        WebLoadingListenerManager.getInstance().registerListener(this)
        // Loading Animate
        loadingDialog = DialogLoading()
        loadingDialog!!.show(activity!!.supportFragmentManager , "loading dialog")

        initView()

        //        binding.search.viewGoWhere.setOnClickListener {
        //            MainActivity.replaceFragment(activity , FragmentGoWhere())
        //        }

        // 開啟網頁設定
        //支持javascript
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.settings.domStorageEnabled = true
        // 設置可以支持缩放
        binding.webview.settings.setSupportZoom(true)
        binding.webview.loadUrl(url)

        return binding.root
    }


    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }
    }

    override fun notifyViewCancelCall() {
        loadingDialog!!.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        WebLoadingListenerManager.getInstance().unRegisterListener(this)
    }

}