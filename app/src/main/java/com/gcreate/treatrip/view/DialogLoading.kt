package com.gcreate.treatrip.view

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.gcreate.treatrip.listener.InterruptCallControlListenerManager
import com.gcreate.treatrip.R
import com.gcreate.treatrip.tools.Global


class DialogLoading : DialogFragment() {


    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog!!.window!!.setLayout(Global.windowWidth , Global.windowHeight )
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        val view: View = inflater.inflate(R.layout.dialog_loading , container , false)

        //  Loading 過程中. User 手動按下物理返回鍵， 防呆處理。
        dialog!!.setOnKeyListener { dialog: DialogInterface , keyCode: Int , event: KeyEvent? ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                InterruptCallControlListenerManager.getInstance()?.sendBroadCast()
                dialog.cancel()
                return@setOnKeyListener true
            }
            false
        }
        return view
    }

    fun dismissDialog() {
        if (dialog != null) dialog!!.dismiss()
    }


}