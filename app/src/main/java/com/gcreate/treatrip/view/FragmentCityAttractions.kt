package com.gcreate.treatrip.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.CityRecommend1Adapter
import com.gcreate.treatrip.databinding.FragmentCityAttractionsBinding
import com.gcreate.treatrip.fakedata.AttractionInfoModel
import com.gcreate.treatrip.fakedata.DataBean
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectCityRecommendPosition
import com.gcreate.treatrip.webAPI.Town
import com.google.android.material.tabs.TabLayout
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

//  首頁點城市進入後的景點推薦
class FragmentCityAttractions : Fragment() {

    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var binding: FragmentCityAttractionsBinding
    private val city: String by Arg("cityName" , "")
    private val cityID: Int by Arg("cityID" , 0)
    private var localCityImage : Int =0

    companion object {
        var objectCityRecommendPosition: ApiObjectCityRecommendPosition? = null
        var currentDataCityName: String? = ""
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_city_attractions , container , false)

//        for (i in DataBean.cityList.indices){
//            if (city.equals(DataBean.cityList[i].cityName_ch)){
//                localCityImage = DataBean.cityList[i].imageRes!!
//            }
//        }




        initView()

        if (currentDataCityName != city) {
            getServerDate()
        } else {
            Glide.with(requireActivity()).load(objectCityRecommendPosition!!.region_banner_img).into(binding.mImgView)
            val list: List<Town> = objectCityRecommendPosition!!.towns
            //  TabLayout 加標籤
            for (element in list) {
                binding.tabs.addTab(binding.tabs.newTab().setText(element.township))
            }
            setCityAttractionsRecyclerView(list)
        }

        //  Recyclerview 滑動.，改變 Tab selected position
        binding.rvCity.setOnScrollChangeListener(View.OnScrollChangeListener { v , scrollX , scrollY , oldScrollX , oldScrollY ->

            binding.tabs.setScrollPosition(linearLayoutManager.findFirstVisibleItemPosition() , 0F , true)

            //            binding.tabs.getTabAt(linearLayoutManager.findFirstVisibleItemPosition())!!.select()

        })

        // 當Tab selected 時，滾動到指定位置
        binding.tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                linearLayoutManager.scrollToPositionWithOffset(binding.tabs.selectedTabPosition , 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                linearLayoutManager.scrollToPositionWithOffset(binding.tabs.selectedTabPosition , 0)
            }

        })


        return binding.root
    }


    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)


        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

        val title = city + "景點推薦"
        binding.tvCityName.text = title
        linearLayoutManager = LinearLayoutManager(activity , LinearLayoutManager.VERTICAL , false)
        binding.rvCity.layoutManager = linearLayoutManager
    }

    private fun getServerDate() {
        currentDataCityName = city

        val parm = JSONObject()
        parm.put("region" , city)
        parm.put("id" , cityID)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , parm.toString())

        Global.treatripAPI.getCityRecommendPosition(requestBody).enqueue(object : Callback<ApiObjectCityRecommendPosition> {
            override fun onResponse(call: Call<ApiObjectCityRecommendPosition> , response: Response<ApiObjectCityRecommendPosition>) {
                objectCityRecommendPosition = response.body()!!

                Glide.with(activity!!).load(objectCityRecommendPosition!!.region_banner_img).into(binding.mImgView)

                val list: List<Town> = objectCityRecommendPosition!!.towns
                //  TabLayout 加標籤
                for (element in list) {
                    binding.tabs.addTab(binding.tabs.newTab().setText(element.township))
                }
                setCityAttractionsRecyclerView(list)
            }

            override fun onFailure(call: Call<ApiObjectCityRecommendPosition> , t: Throwable) {
                TODO("Not yet implemented")
            }

        })

    }

    private fun setCityAttractionsRecyclerView(list: List<Town>) {

        val cityAdapter = CityRecommend1Adapter(activity , binding.rvCity , list)
        binding.rvCity.adapter = cityAdapter

    }

}

