package com.gcreate.treatrip.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.TaiwanExplore1Adapter
import com.gcreate.treatrip.databinding.FragmentTaiwanExploreBinding
import com.gcreate.treatrip.listener.InterruptCallControlListener
import com.gcreate.treatrip.listener.InterruptCallControlListenerManager
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectTaiwanExplore
import com.gcreate.treatrip.webAPI.DiscoverTaiwain
import com.google.android.material.tabs.TabLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentTaiwanExplore : Fragment() , InterruptCallControlListener {

    //  探索台灣
    private lateinit var binding: FragmentTaiwanExploreBinding
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var loadingDialog: DialogLoading
    private lateinit var call: Call<ApiObjectTaiwanExplore>

    companion object {
        var objectThemeAdventure: ApiObjectTaiwanExplore? = null
        var currentDataCityName: String? = ""
    }

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_taiwan_explore , container , false)
        InterruptCallControlListenerManager.getInstance()?.registerListener(this)
        initView()

        // Loafing Animate
        loadingDialog = DialogLoading()
        loadingDialog.show(activity!!.supportFragmentManager , "simple dialog")

        processThread()

        //  Recyclerview 滑動.，改變 Tab selected position
        binding.rvContent.setOnScrollChangeListener(View.OnScrollChangeListener { v , scrollX , scrollY , oldScrollX , oldScrollY ->

            binding.tabs.setScrollPosition(linearLayoutManager.findFirstVisibleItemPosition() , 0F , true)

            //            binding.tabs.getTabAt(linearLayoutManager.findFirstVisibleItemPosition())!!.select()

        })

        // 當Tab selected 時，滾動到指定位置
        binding.tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                linearLayoutManager.scrollToPositionWithOffset(binding.tabs.selectedTabPosition , 0)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                linearLayoutManager.scrollToPositionWithOffset(binding.tabs.selectedTabPosition , 0)
            }

        })

        return binding.root
    }

    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)


        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

        linearLayoutManager = LinearLayoutManager(activity , LinearLayoutManager.VERTICAL , false)
        binding.rvContent.layoutManager = linearLayoutManager
    }

    private fun processThread() {
        //构建一个下载进度条
        object : Thread() {
            override fun run() {
                //在新线程里执行长耗时方法
                getServerDate()
            }
        }.start()
    }

    private val handler: Handler = @SuppressLint("HandlerLeak") object : Handler() {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)
            //只要执行到这里就关闭对话框
            loadingDialog.dismissDialog()
        }
    }

    private fun getServerDate() {
        call = Global.treatripAPI.getTaiwanExploreInfo()
        call.enqueue(object : Callback<ApiObjectTaiwanExplore> {
            override fun onResponse(call: Call<ApiObjectTaiwanExplore> , response: Response<ApiObjectTaiwanExplore>) {
                objectThemeAdventure = response.body()!!
                val list: MutableList<DiscoverTaiwain> = objectThemeAdventure!!.discover_taiwain
                //  TabLayout 加標籤
                for (element in list) {
                    binding.tabs.addTab(binding.tabs.newTab().setText(element.main_type))
                }
                //执行完毕后给handler发送一个空消息關閉 Loading Dialog
                handler.sendEmptyMessage(0)
                setTaiwanExploreRecyclerView(list)
            }

            override fun onFailure(call: Call<ApiObjectTaiwanExplore> , t: Throwable) {

            }

        })
    }

    private fun setTaiwanExploreRecyclerView(list: MutableList<DiscoverTaiwain>) {

        val taiwanExploreAdapter = TaiwanExplore1Adapter(activity!! , R.layout.card_item_taiwan_explore_1 , list)
        binding.rvContent.adapter = taiwanExploreAdapter
        taiwanExploreAdapter.addFooterView(layoutInflater.inflate(R.layout.item_footer, null))


    }

    override fun notifyViewCancelCall() {
        call.cancel()
        activity!!.onBackPressed()
    }

    override fun onDestroy() {
        super.onDestroy()
        InterruptCallControlListenerManager.getInstance()!!.unRegisterListener(this)
    }
}