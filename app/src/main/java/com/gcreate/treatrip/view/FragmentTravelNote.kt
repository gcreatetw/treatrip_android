package com.gcreate.treatrip.view

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.get
import androidx.core.view.setMargins
import androidx.core.widget.NestedScrollView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemClickListener
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.TravelNoteBloggerAdapter
import com.gcreate.treatrip.adapters.TravelNoteFeaturedAdapter
import com.gcreate.treatrip.adapters.TravelNoteRecommendAdapter
import com.gcreate.treatrip.databinding.FragmentTravelNoteBinding
import com.gcreate.treatrip.fakedata.DataBean
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.tools.CustomLoadMoreView
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.*
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**遊記*/
class FragmentTravelNote : Fragment() , ApiLoadCompleteCallback {

    private lateinit var binding: FragmentTravelNoteBinding
    private lateinit var objectTravelNote: ApiObjectTravelNote
    private lateinit var mAdapter: TravelNoteRecommendAdapter
    private var travelRecommendArrayList: ArrayList<TravelRecommend> = ArrayList()
    private var currentPage: Int = 1

    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_travel_note , container , false)

        initBanner()
        initViewPosition()
        callTravelNoteAPI()
        callTravelRecommendAPI(currentPage)
        nestScrollViewListener()


        //  想趣哪裡
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

        return binding.root
    }


    private fun initBanner() {
        //设置banner样式
        val mBanner = binding.componentBanner.banner
        mBanner.layoutParams.height = ((Global.windowHeight * 0.34).toInt())

        /*
        * 内置的PageTransformer
        * AlphaPageTransformer  /   DepthPageTransformer    /   RotateDownPageTransformer   /   RotateUpPageTransformer
        *RotateYTransformer /   ScaleInTransformer   /  ZoomOutPageTransformer
        * */
        //设置图片加载器
        mBanner.setAdapter(object : BannerImageAdapter<DataBean>(DataBean.bannerImages(activity)) {
            override fun onBindView(holder: BannerImageHolder , data: DataBean , position: Int , size: Int) {
                Glide.with(holder.itemView).load(data.imageRes).into(holder.imageView)
            }
        }).addBannerLifecycleObserver(this).indicator = CircleIndicator(activity)
        // 輪播方式
        mBanner.setPageTransformer(AlphaPageTransformer())
            // Indicator parameters
            .setIndicatorSelectedColor(Color.WHITE).setIndicatorSpace(16)
            // Indicator parameters
            .start()

    }

    private fun initViewPosition() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()

        binding.search.iconArrow.visibility = View.GONE

        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        // 設定透明框的高度
        //        val params = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT , ((Global.windowHeight * 0.3).toInt()))
        //        binding.transparentView.layoutParams = params

        binding.rvTravelRecommend.imgTitle.setImageResource(R.drawable.icon_travel_recommend)
        binding.rvTravelRecommend.tvTitle.text = "遊記推薦"

        // recycler view Data binding
        binding.rvTravelRecommend.componentRv.layoutManager = GridLayoutManager(activity , 2 , GridLayoutManager.VERTICAL , false)


        mAdapter = TravelNoteRecommendAdapter(requireActivity() , R.layout.card_item_time_interval , travelRecommendArrayList)
        binding.rvTravelRecommend.componentRv.adapter = mAdapter

        // 获取模块
        mAdapter.loadMoreModule
        mAdapter.loadMoreModule.loadMoreView = CustomLoadMoreView()
        // 打开或关闭加载更多功能（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMore = true
        // 是否自定加载下一页（默认为true）
        mAdapter.loadMoreModule.isAutoLoadMore = true
        // 当数据不满一页时，是否继续自动加载（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false
        // 所有数据加载完成后，是否允许点击（默认为false）
        mAdapter.loadMoreModule.enableLoadMoreEndClick = false
        // 是否处于加载中
        mAdapter.loadMoreModule.isLoading

        mAdapter.setOnItemClickListener(object : OnItemClickListener {
            override fun onItemClick(adapter: BaseQuickAdapter<* , *> , view: View , position: Int) {
                Log.d("Ben" , "position = " + position)
                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL" , travelRecommendArrayList[position].post_url)
                webView.arguments = bundle
                MainActivity.replaceFragment(activity , webView)
            }
        })


    }

    private fun callTravelNoteAPI() {
        Global.treatripAPI.getTravelNoteInfo().enqueue(object : Callback<ApiObjectTravelNote> {

            override fun onResponse(call: Call<ApiObjectTravelNote> , response: Response<ApiObjectTravelNote>) {
                objectTravelNote = response.body()!!
                loadComplete()
            }

            override fun onFailure(call: Call<ApiObjectTravelNote> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun loadComplete() {
        setFeaturedTravelNotesRecyclerView()
        setBloggerRecyclerView()
    }

    // 精選遊記
    private fun setFeaturedTravelNotesRecyclerView() {
        binding.rvTravelNote.lyRv2.setBackgroundColor(resources.getColor(R.color.pink_ed7465))
        binding.rvTravelNote.imgTitle.setImageResource(R.drawable.icon_popular_attractions)
        binding.rvTravelNote.tvTitle.text = "精選遊記"
        binding.rvTravelNote.tvTitle.setTextColor(resources.getColor(R.color.white))
        binding.rvTravelNote.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)
        val featuredAdapter = TravelNoteFeaturedAdapter(activity , objectTravelNote.playmate)
        binding.rvTravelNote.componentRv.adapter = featuredAdapter

        featuredAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {

                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL" , objectTravelNote.playmate[position].post_url)
                webView.arguments = bundle
                MainActivity.replaceFragment(activity , webView)
            }
        })

    }

    // Blogger
    private fun setBloggerRecyclerView() {
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins((Global.windowWidth * 0.085).toInt() , (Global.windowHeight * 0.025).toInt() , 0 , 0)
        binding.rvBlogger.componentRv.layoutParams = params

        binding.rvBlogger.componentRv.layoutManager = GridLayoutManager(activity , 2 , GridLayoutManager.HORIZONTAL , false)
        val bloggerAdapter = TravelNoteBloggerAdapter(activity , objectTravelNote.blogger)
        binding.rvBlogger.componentRv.adapter = bloggerAdapter

        bloggerAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                replaceFragment(activity , FragmentBlogger() , objectTravelNote.blogger[position])
            }
        })
    }

    // 遊記推薦
    lateinit var objectTravelRecommend: ApiObjectTravelRecommend

    private fun callTravelRecommendAPI(Page: Int) {

        val params = JSONObject()
        params.put("page" , Page)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , params.toString())

        Global.treatripAPI.getTravelRecommendInfo(requestBody).enqueue(object : Callback<ApiObjectTravelRecommend> {

            override fun onResponse(call: Call<ApiObjectTravelRecommend> , response: Response<ApiObjectTravelRecommend>) {
                objectTravelRecommend = response.body()!!
                if (objectTravelRecommend.travel_recommend.size < 20) {
                    currentPage--
                    /******************************** 状态设置 ********************************/
                    // 当前这次数据加载完毕，调用此方法
                    mAdapter.loadMoreModule.loadMoreComplete()
                    // 所有数据加载完成，调用此方法
                    // 需要重置"加载完成"状态时，请调用 setNewData()
                    mAdapter.loadMoreModule.loadMoreEnd()
                }
                populateData(objectTravelRecommend)
            }

            override fun onFailure(call: Call<ApiObjectTravelRecommend> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun populateData(objectTravelRecommend: ApiObjectTravelRecommend) {

        for (i in 0 until objectTravelRecommend.travel_recommend.size) {
            travelRecommendArrayList.add(objectTravelRecommend.travel_recommend[i])
        }

        mAdapter.setNewInstance(travelRecommendArrayList)
        mAdapter.notifyDataSetChanged()
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false

        // 预加载的位置（默认为1）
        mAdapter.loadMoreModule.preLoadNumber = currentPage

        mAdapter.loadMoreModule.setOnLoadMoreListener {}

    }

    var alreadyAddfooter = false
    private fun nestScrollViewListener() {
        binding.scrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v , scrollX , scrollY , oldScrollX , oldScrollY ->
            val scrollY: Int = binding.scrollView.scrollY
            val onlyChild: View = binding.scrollView.getChildAt(0)
            if (currentPage < 3) {
                if (onlyChild.height <= scrollY + binding.scrollView.height) {   // 如果满足就是到底部了
                    currentPage++
                    callTravelRecommendAPI(currentPage)
                }
            } else {
                if (!alreadyAddfooter) {
                    mAdapter.addFooterView(layoutInflater.inflate(R.layout.item_footer2 , null))
                    mAdapter.notifyDataSetChanged()
                    /******************************** 状态设置 ********************************/
                    // 当前这次数据加载完毕，调用此方法
                    mAdapter.loadMoreModule.loadMoreComplete()
                    // 所有数据加载完成，调用此方法
                    // 需要重置"加载完成"状态时，请调用 setNewData()
                    mAdapter.loadMoreModule.loadMoreEnd()
                    mAdapter.loadMoreModule.isEnableLoadMore = false
                    alreadyAddfooter = true

                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
                    params.setMargins(30)
                    mAdapter.footerLayout?.layoutParams = params

                    val btnMore = mAdapter.footerLayout?.get(0)?.findViewById<View>(R.id.btn_more) as Button
                    btnMore.setOnClickListener {
                        resetArticle()
                        MainActivity.replaceFragment(activity , FragmentMoreArticle())
                    }
                }
            }
        })
    }


    private fun replaceFragment(mainActivity: FragmentActivity? , fragment: Fragment , objectBlogger: Blogger) {
        val fragmentManager = mainActivity?.supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        val bundle = Bundle()
        bundle.putString("bloggerID" , objectBlogger.ID)
        bundle.putString("bloggerImg" , objectBlogger.post_img)
        bundle.putString("bloggerName" , objectBlogger.post_title)
        fragment.arguments = bundle
        fragmentTransaction?.replace(R.id.container , fragment , "")
        fragmentTransaction?.addToBackStack(fragment.toString())
        fragmentTransaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction?.commit()
    }

    private fun resetArticle() {
        currentPage = 1
        travelRecommendArrayList.removeAll(travelRecommendArrayList)
        alreadyAddfooter = false
    }
}

