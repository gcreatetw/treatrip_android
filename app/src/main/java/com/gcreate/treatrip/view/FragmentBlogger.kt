package com.gcreate.treatrip.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.treatrip.MainActivity.Companion.replaceFragment
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.BloggerAdapter
import com.gcreate.treatrip.databinding.FragmentBloggerBinding
import com.gcreate.treatrip.tools.Arg
import com.gcreate.treatrip.tools.GlideCircleTransform
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectBlogger
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentBlogger : Fragment() {

    private lateinit var binding: FragmentBloggerBinding
    private lateinit var objectBlogger: ApiObjectBlogger
    private lateinit var mAdapter: BloggerAdapter

    private val bloggerID: String by Arg("bloggerID" , "")
    private val bloggerImg: String by Arg("bloggerImg" , "")
    private val bloggerName: String by Arg("bloggerName" , "")


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_blogger , container , false)

        initView()
        callBloggerAPI(bloggerID.toInt())


        return binding.root
    }


    private fun initView() {

        binding.search.iconArrow.setOnClickListener { activity!!.onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            replaceFragment(activity , FragmentGoWhere())
        }
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        Glide.with(activity!!).load(bloggerImg).placeholder(R.drawable.circle_graph).transform(GlideCircleTransform(activity , 2 , activity!!.resources.getColor(R.color.pink_ed7465)))
            .into(binding.itemBloggerImage)
        binding.itemBloggerName.text = bloggerName

        // recycler view Data binding
        val layoutManager = StaggeredGridLayoutManager(2 , StaggeredGridLayoutManager.VERTICAL)
        //        layoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE    //  防止瀑布流 item交换位置
        binding.rvContent.layoutManager = layoutManager
        binding.rvContent.isNestedScrollingEnabled = false

    }

    private fun callBloggerAPI(ID: Int) {

        val params = JSONObject()
        params.put("post_id" , ID)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , params.toString())

        Global.treatripAPI.getBloggerInfo(requestBody).enqueue(object : Callback<ApiObjectBlogger> {

            override fun onResponse(call: Call<ApiObjectBlogger> , response: Response<ApiObjectBlogger>) {
                objectBlogger = response.body()!!

                binding.itemBloggerWeb.text = objectBlogger.bloger_url
                mAdapter = BloggerAdapter(activity!! , R.layout.card_item_theme_adventure , objectBlogger.bloger_posts)
                binding.rvContent.adapter = mAdapter

            }

            override fun onFailure(call: Call<ApiObjectBlogger> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })

    }


}