package com.gcreate.treatrip.view

import android.Manifest
import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Geocoder
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.MainActivity.Companion.replaceFragment
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.HomeAttractionsCityAdapter
import com.gcreate.treatrip.adapters.HomePopularAttractionsAdapter
import com.gcreate.treatrip.adapters.HomeTimeIntervalAdapter
import com.gcreate.treatrip.databinding.FragmentAttractionsBinding
import com.gcreate.treatrip.fakedata.DataBean
import com.gcreate.treatrip.listener.ItemClickListener
import com.gcreate.treatrip.tools.AppBarStateChangeListener
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.tools.util.PermissionCheckUtil
import com.gcreate.treatrip.webAPI.ApiLoadCompleteCallback
import com.gcreate.treatrip.webAPI.ApiObjectHomeInfo
import com.google.android.gms.location.*
import com.google.android.material.appbar.AppBarLayout
import com.youth.banner.adapter.BannerImageAdapter
import com.youth.banner.config.IndicatorConfig
import com.youth.banner.holder.BannerImageHolder
import com.youth.banner.indicator.CircleIndicator
import com.youth.banner.transformer.AlphaPageTransformer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList


/**首頁景點*/
class FragmentAttractions : Fragment() , ApiLoadCompleteCallback , AppBarLayout.OnOffsetChangedListener {

    private val TAG = "HomePage"
    private lateinit var binding: FragmentAttractionsBinding
    private lateinit var objectHomeInfo: ApiObjectHomeInfo
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback

    companion object {
        var bannerImgList: List<String> = ArrayList()
        var userLocationCity: String = "台北市"
    }


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_attractions , container , false)

        requestPermission()
        initViewPosition()
        callHomeAPI()
        setCityRecyclerView()
        viewAction()


        //        val location = GPSUtils.getInstance(requireContext())
        //        Log.d("ben" , "location = $location")
        //        location!!.getLngAndLat(object : GPSUtils.OnLocationResultListener {
        //            override fun onLocationResult(location: Location?) {
        //                val gc = Geocoder(activity , Locale.TAIWAN)
        //                val lstAddress: List<Address> = gc.getFromLocation(location!!.latitude , location.longitude , 1)
        //                Log.d("ben" , "location!!.latitude = ${location!!.latitude}")
        //                Log.d("ben" , "location!!.latitude = ${location!!.longitude}")
        //                //  取得縣市
        //                userLocationCity = lstAddress[0].subAdminArea
        //                Log.d(TAG , userLocationCity)
        //            }
        //
        //            override fun onLocationChange(location: Location?) {
        //                val gc = Geocoder(activity , Locale.TRADITIONAL_CHINESE)
        //                val lstAddress: List<Address> = gc.getFromLocation(location!!.latitude , location.longitude , 1)
        //                //  取得縣市
        //                userLocationCity = lstAddress[0].subAdminArea
        //                Log.d(TAG , userLocationCity)
        //            }
        //
        //        })

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {

                    val addresses = Geocoder(requireContext() , Locale.getDefault()).getFromLocation(location.latitude , location.longitude , 1)
                    val address = addresses[0]
                    userLocationCity = if (address.subAdminArea != null) {
                        address.subAdminArea
                    } else {
                        address.adminArea
                    }
                }
            }
        }


        return binding.root
    }


    private fun initBanner() {
        val mBanner = binding.componentBanner.banner
        mBanner.layoutParams.height = ((Global.windowHeight * 0.34).toInt())
        val list: MutableList<DataBean> = ArrayList()
        for (element in objectHomeInfo.banner_img) {
            list.add(DataBean(element , null , 1))
        }

        /*
                * 内置的PageTransformer
                * AlphaPageTransformer  /   DepthPageTransformer    /   RotateDownPageTransformer   /   RotateUpPageTransformer
                * RotateYTransformer /  ScaleInTransformer   /  ZoomOutPageTransformer
                * */ //设置图片加载器
        mBanner.setAdapter(object : BannerImageAdapter<DataBean>(list) {
            override fun onBindView(holder: BannerImageHolder , data: DataBean , position: Int , size: Int) {
                Glide.with(holder.itemView).load(data.imageUrl).error(R.mipmap.home_bn01).into(holder.imageView)
            }
        }).addBannerLifecycleObserver(this).indicator = CircleIndicator(activity) // 輪播方式
        mBanner.setPageTransformer(AlphaPageTransformer()) // Indicator parameters
            .setIndicatorSelectedColor(Color.WHITE).setIndicatorSpace(16).setIndicatorMargins(IndicatorConfig.Margins((Global.windowHeight * 0.05).toInt())) // Indicator parameters
            .start()

    }

    private fun initViewPosition() { // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()

        binding.search.iconArrow.visibility = View.GONE

        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        // 設定透明框的位置
        val params = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT , ((Global.windowHeight * 0.3).toInt()))
        binding.transparentView.layoutParams = params

        binding.mAppBarLayout.addOnOffsetChangedListener(object : AppBarStateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout? , state: State? , scrollY: Int) {
                when (state) {
                    State.EXPANDED -> { //展开状态
                    }
                    State.COLLAPSED -> { //折叠状态
                        binding.nestView.setBackgroundColor(Color.WHITE)
                    }
                    else -> { //中间状态
                        binding.nestView.background = ContextCompat.getDrawable(activity!! , R.drawable.solid_top_r24_white_ffffff)
                    }
                }
            }
        })
    }

    private fun setCityRecyclerView() {
        val cityAdapter = HomeAttractionsCityAdapter(activity , DataBean.cityList)
        binding.rvCity.componentRv.adapter = cityAdapter
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT , LinearLayout.LayoutParams.WRAP_CONTENT)
        params.setMargins((Global.windowWidth * 0.05).toInt() , (Global.windowHeight * 0.025).toInt() , 0 , 0)
        binding.rvCity.componentRv.layoutParams = params
        binding.rvCity.componentRv.layoutManager = GridLayoutManager(activity , 2 , GridLayoutManager.HORIZONTAL , false)

        cityAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                MainActivity.currentFragmentName = "FragmentCityAttractions"
                replaceFragment(activity , FragmentCityAttractions() , DataBean.cityList[position])
            }
        })

    }

    private fun callHomeAPI() {
        Global.treatripAPI.getHomeInfo().enqueue(object : Callback<ApiObjectHomeInfo> {

            override fun onResponse(call: Call<ApiObjectHomeInfo> , response: Response<ApiObjectHomeInfo>) {
                objectHomeInfo = response.body()!!
                bannerImgList = objectHomeInfo.banner_img
                loadComplete()
            }

            override fun onFailure(call: Call<ApiObjectHomeInfo> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun loadComplete() {
        initBanner()
        setTimeIntervalRecyclerView()
        setPopularAttractionsRecyclerView()
    }

    // 期間限定
    private fun setTimeIntervalRecyclerView() {
        binding.rvIntervalLimit.imgTitle.setImageResource(R.drawable.icon_time_interval)
        binding.rvIntervalLimit.tvTitle.text = "期間限定"
        binding.rvIntervalLimit.tvTitle
        binding.rvIntervalLimit.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)

        val timeIntervalAdapter = HomeTimeIntervalAdapter(activity , objectHomeInfo.limited_time)
        binding.rvIntervalLimit.componentRv.adapter = timeIntervalAdapter
        timeIntervalAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL" , objectHomeInfo.popular_attractions[position].post_url)
                webView.arguments = bundle
                replaceFragment(activity , webView)
            }
        })

    }

    // 爆紅景點
    private fun setPopularAttractionsRecyclerView() {
        binding.rvPopularAttractions.lyRv2.setBackgroundColor(ContextCompat.getColor(requireActivity() , R.color.pink_ed7465))
        binding.rvPopularAttractions.imgTitle.setImageResource(R.drawable.icon_popular_attractions)
        binding.rvPopularAttractions.tvTitle.text = "爆紅景點"
        binding.rvPopularAttractions.tvTitle.setTextColor(ContextCompat.getColor(requireActivity() , R.color.white))
        binding.rvPopularAttractions.componentRv.layoutManager = LinearLayoutManager(activity , LinearLayoutManager.HORIZONTAL , false)
        val popularAttractionsAdapter = HomePopularAttractionsAdapter(activity , objectHomeInfo.popular_attractions)
        binding.rvPopularAttractions.componentRv.adapter = popularAttractionsAdapter

        popularAttractionsAdapter.setOnKotlinItemClickListener(object : ItemClickListener {
            override fun onItemClickListener(position: Int) {
                val webView = FragmentWebView()
                val bundle = Bundle()
                bundle.putString("LinkURL" , objectHomeInfo.popular_attractions[position].post_url)
                webView.arguments = bundle
                replaceFragment(activity , webView)
            }
        })
    }

    private fun viewAction() {

        //  想趣哪裡
        binding.search.viewGoWhere.setOnClickListener {
            replaceFragment(activity , FragmentGoWhere())
        }

        //  主題冒險
        binding.clTheme1.setOnClickListener {
            replaceFragment(activity , FragmentThemeAdventure())
        }

        //  探索台灣
        binding.clTheme2.setOnClickListener {
            replaceFragment(activity , FragmentTaiwanExplore())
        }
    }

    private fun replaceFragment(mainActivity: FragmentActivity? , fragment: Fragment , dataBean: DataBean) {
        val fragmentManager = mainActivity?.supportFragmentManager
        val fragmentTransaction = fragmentManager?.beginTransaction()
        val bundle = Bundle()
        bundle.putString("cityName" , dataBean.cityName_ch)
        bundle.putInt("cityID" , dataBean.cityID)
        fragment.arguments = bundle
        fragmentTransaction?.replace(R.id.container , fragment , "")
        fragmentTransaction?.addToBackStack(fragment.toString())
        fragmentTransaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction?.commit()
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout? , verticalOffset: Int) {
        Log.d("ben" , "verticalOffset = " + verticalOffset);
    }


    /**
     * checkPermission
     */
    val pVerifyUtil = PermissionCheckUtil(context , null)
    var permissionsArray = arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION , Manifest.permission.WRITE_EXTERNAL_STORAGE)
    private fun requestPermission() {
        requestPermissions(permissionsArray , PermissionCheckUtil.ONCE_TIME_APPLY)
    }

    /**
     * 请求权限结果回调
     * @param requestCode
     * @param permissions
     * @param grantResults
     */

    override fun onRequestPermissionsResult(requestCode: Int , permissions: Array<out String> , grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode , permissions , grantResults) //        when (requestCode) {
        //            PVerifyUtil.ONCE_TIME_APPLY -> pVerifyUtil.onceTimeApplyResult(permissionsArray , grantResults)
        //        }
        getUserLocation()
    }


    /**
     * 定位 method 1. 容易 grpc error
     * */
    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        fusedLocationClient.requestLocationUpdates(LocationRequest.create() , locationCallback , Looper.getMainLooper())
    }


    override fun onDestroy() {
        super.onDestroy()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}