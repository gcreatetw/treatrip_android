package com.gcreate.treatrip.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.adapters.TravelNoteLoadMoreArticleAdapter
import com.gcreate.treatrip.databinding.FragmentMoreArticleBinding
import com.gcreate.treatrip.tools.CustomLoadMoreView
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.webAPI.ApiObjectLoadMoreArticle
import com.gcreate.treatrip.webAPI.ApiObjectLoadMoreArticleCategory
import com.gcreate.treatrip.webAPI.Article
import com.google.android.material.tabs.TabLayout
import okhttp3.MediaType
import okhttp3.RequestBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FragmentMoreArticle : Fragment() {

    private lateinit var binding: FragmentMoreArticleBinding
    private lateinit var mAdapter: TravelNoteLoadMoreArticleAdapter
    private lateinit var objectTravelRecommend: ApiObjectLoadMoreArticle
    private lateinit var objectLoadMoreArticleCategory: ApiObjectLoadMoreArticleCategory
    private lateinit var callArticleAPI: Call<ApiObjectLoadMoreArticle>
    private lateinit var travelRecommendArrayList: ArrayList<Article>
    
    private var currentPage: Int = 1
    private var currentArticleCategoryID = 0


    override fun onCreateView(inflater: LayoutInflater , container: ViewGroup? , savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater , R.layout.fragment_more_article , container , false)

        travelRecommendArrayList = ArrayList()

        initViewPosition()
        callArticleCategoryAPI()
        recyclerViewListener()

        return binding.root
    }


    private fun initViewPosition() {

        (activity as AppCompatActivity).setSupportActionBar(binding.mToolBar)
        binding.search.iconArrow.setOnClickListener { requireActivity().onBackPressed() }
        binding.search.viewGoWhere.setOnClickListener {
            MainActivity.replaceFragment(activity , FragmentGoWhere())
        }

        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()

        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        // 設定透明框的高度
        //        val params = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT , ((Global.windowHeight * 0.3).toInt()))
        //        binding.transparentView.layoutParams = params


        // recycler view Data binding
        binding.rvTravelRecommend.layoutManager = GridLayoutManager(activity , 2 , GridLayoutManager.VERTICAL , false)

        mAdapter = TravelNoteLoadMoreArticleAdapter(requireActivity() , R.layout.card_item_time_interval , travelRecommendArrayList)
        binding.rvTravelRecommend.adapter = mAdapter

        // 获取模块
        mAdapter.loadMoreModule
        mAdapter.loadMoreModule.loadMoreView = CustomLoadMoreView() // 打开或关闭加载更多功能（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMore = true // 是否自定加载下一页（默认为true）
        mAdapter.loadMoreModule.isAutoLoadMore = true // 当数据不满一页时，是否继续自动加载（默认为true）
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false // 所有数据加载完成后，是否允许点击（默认为false）
        mAdapter.loadMoreModule.enableLoadMoreEndClick = false // 是否处于加载中
        mAdapter.loadMoreModule.isLoading

        mAdapter.setOnItemClickListener { _ , _ , position ->
            val webView = FragmentWebView()
            val bundle = Bundle()
            bundle.putString("LinkURL" , travelRecommendArrayList[position].post_url)
            webView.arguments = bundle
            MainActivity.replaceFragment(activity , webView)
        }

        binding.moreArticleTabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) { //                callArticleAPI.cancel()
                travelRecommendArrayList.clear()
                travelRecommendArrayList = ArrayList()
                currentPage = 1
                currentArticleCategoryID = objectLoadMoreArticleCategory.categories[tab!!.position].id
                mAdapter.setNewData(travelRecommendArrayList)

                callTravelRecommendAPI(currentArticleCategoryID , currentPage)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

    }

    private fun callArticleCategoryAPI() {
        Global.treatripAPI.getArticleCategory().enqueue(object : Callback<ApiObjectLoadMoreArticleCategory> {
            override fun onResponse(call: Call<ApiObjectLoadMoreArticleCategory> , response: Response<ApiObjectLoadMoreArticleCategory>) {
                objectLoadMoreArticleCategory = response.body()!! //  TabLayout 加標籤
                for (element in objectLoadMoreArticleCategory.categories) {
                    binding.moreArticleTabs.addTab(binding.moreArticleTabs.newTab().setText(element.name))
                }
                currentArticleCategoryID = objectLoadMoreArticleCategory.categories[0].id

            }

            override fun onFailure(call: Call<ApiObjectLoadMoreArticleCategory> , t: Throwable) {
                TODO("Not yet implemented")
            }
        })

    }

    private fun callTravelRecommendAPI(categoryID: Int , Page: Int) {

        val params = JSONObject()
        params.put("id" , categoryID)
        params.put("page" , Page)
        val requestBody = RequestBody.create(MediaType.parse("application/json") , params.toString())

        callArticleAPI = Global.treatripAPI.getLoadMoreArticle(requestBody)
        callArticleAPI.enqueue(object : Callback<ApiObjectLoadMoreArticle> {

            override fun onResponse(call: Call<ApiObjectLoadMoreArticle> , response: Response<ApiObjectLoadMoreArticle>) {
                objectTravelRecommend = response.body()!!
                if (objectTravelRecommend.posts.size < 10) {

                    /******************************** 状态设置 ********************************/ // 当前这次数据加载完毕，调用此方法
                    mAdapter.loadMoreModule.loadMoreComplete() // 所有数据加载完成，调用此方法
                    // 需要重置"加载完成"状态时，请调用 setNewData()
                    mAdapter.loadMoreModule.loadMoreEnd()
                }

                populateData(objectTravelRecommend)
            }

            override fun onFailure(call: Call<ApiObjectLoadMoreArticle> , t: Throwable) {
                Toast.makeText(activity , "資料獲取失敗，請確認網路連線" , Toast.LENGTH_SHORT).show()
            }

        })
    }

    private fun populateData(objectTravelRecommend: ApiObjectLoadMoreArticle) {

        for (element in objectTravelRecommend.posts) {
            travelRecommendArrayList.add(element)
        }

        mAdapter.setNewInstance(travelRecommendArrayList)
        mAdapter.notifyDataSetChanged()
        mAdapter.loadMoreModule.isEnableLoadMoreIfNotFullPage = false

        // 预加载的位置（默认为1）
        mAdapter.loadMoreModule.preLoadNumber = currentPage

        mAdapter.loadMoreModule.setOnLoadMoreListener {}

    }

    private fun recyclerViewListener() {
        binding.rvTravelRecommend.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView , newState: Int) {
                super.onScrollStateChanged(recyclerView , newState)

                //  滾動時不給切換 tab 避免閃退
                if (newState == 0) {
                    val tabStrip = binding.moreArticleTabs.getChildAt(0) as LinearLayout
                    tabStrip.isEnabled = false
                    for (i in 0 until tabStrip.childCount) {
                        tabStrip.getChildAt(i).isClickable = true
                    }
                } else {
                    val tabStrip = binding.moreArticleTabs.getChildAt(0) as LinearLayout
                    tabStrip.isEnabled = true
                    for (i in 0 until tabStrip.childCount) {
                        tabStrip.getChildAt(i).isClickable = false
                    }
                }

            }

            override fun onScrolled(recyclerView: RecyclerView , dx: Int , dy: Int) {
                super.onScrolled(recyclerView , dx , dy)
                val touchableList = binding.moreArticleTabs.touchables //得到当前显示的最后一个item的view

                if (recyclerView.layoutManager!!.getChildAt(recyclerView.layoutManager!!.childCount - 1) == null){
                    val lastChildView =0
                }else{
                    val lastChildView = recyclerView.layoutManager!!.getChildAt(recyclerView.layoutManager!!.childCount - 1)
                    val lastPosition = recyclerView.layoutManager!!.getPosition(lastChildView!!)
                    //判断lastChildView的bottom值跟recyclerBottom
                    // 判断lastPosition是不是最后一个position
                    // 如果两个条件都满足则说明是真正的滑动到了底部
                    if (lastPosition == recyclerView.layoutManager!!.itemCount - 1) {
                        currentPage++
                        callTravelRecommendAPI(currentArticleCategoryID , currentPage)
                    }
                }


                //得到lastChildView的bottom坐标值
//                                val lastChildBottom = lastChildView!!.bottom

                //得到Recyclerview的底部坐标减去底部padding值，也就是显示内容最底部的坐标
//                                val recyclerBottom = recyclerView.bottom - recyclerView.paddingBottom

                //通过这个lastChildView得到这个view当前的position值
//                val lastPosition = recyclerView.layoutManager!!.getPosition(lastChildView!!)
//                //判断lastChildView的bottom值跟recyclerBottom
//                // 判断lastPosition是不是最后一个position
//                // 如果两个条件都满足则说明是真正的滑动到了底部
//                if (lastPosition == recyclerView.layoutManager!!.itemCount - 1) {
//                    currentPage++
//                    callTravelRecommendAPI(currentArticleCategoryID , currentPage)
//                }

            }
        })
    }


}