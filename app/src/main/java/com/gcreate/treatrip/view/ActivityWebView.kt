package com.gcreate.treatrip.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.gcreate.treatrip.MainActivity
import com.gcreate.treatrip.R
import com.gcreate.treatrip.databinding.ActivityWebViewBinding
import com.gcreate.treatrip.listener.WebLoadingListener
import com.gcreate.treatrip.listener.WebLoadingListenerManager
import com.gcreate.treatrip.tools.Global
import com.gcreate.treatrip.tools.lazyExtra

class ActivityWebView : AppCompatActivity() , WebLoadingListener {

    lateinit var  binding: ActivityWebViewBinding
    private var loadingDialog: DialogLoading? = null
    private val url: String by lazyExtra("LinkURL" , "")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this , R.layout.activity_web_view)
        WebLoadingListenerManager.getInstance().registerListener(this)
        // Loading Animate
        loadingDialog = DialogLoading()
        loadingDialog!!.show(this.supportFragmentManager , "loading dialog")

        initView()

//        binding.search.viewGoWhere.setOnClickListener {
//            MainActivity.replaceFragment(activity , FragmentGoWhere())
//        }

        // 開啟網頁設定
        //支持javascript
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.settings.domStorageEnabled = true
        // 設置可以支持缩放
        binding.webview.settings.setSupportZoom(true)
        binding.webview.loadUrl(url)
    }

    private fun initView() {
        // 設定 想去哪裡 View 的位置
        val marginTopEdge = (Global.windowWidth * 0.05).toInt()
        val marginEdge = (Global.windowWidth * 0.08).toInt()
        val set = ConstraintSet()
        set.clone(binding.cl)
        set.connect(R.id.search , ConstraintSet.TOP , ConstraintSet.PARENT_ID , ConstraintSet.TOP , 0)
        set.connect(R.id.search , ConstraintSet.LEFT , ConstraintSet.PARENT_ID , ConstraintSet.LEFT , marginEdge / 2)
        set.connect(R.id.search , ConstraintSet.RIGHT , ConstraintSet.PARENT_ID , ConstraintSet.RIGHT , marginEdge)
        set.applyTo(binding.cl)

        binding.search.iconArrow.setOnClickListener { finish() }
        binding.search.viewGoWhere.setOnClickListener{
            finish()
            val intent = Intent(this,MainActivity::class.java)
            intent.putExtra("isComeFromWebActivity",true)
            startActivity(intent)
        }
    }

    override fun notifyViewCancelCall() {
        loadingDialog!!.dismiss()
    }

    override fun onDestroy() {
        super.onDestroy()
        WebLoadingListenerManager.getInstance().unRegisterListener(this)
    }


}

