package com.gcreate.treatrip.listener

interface InterruptCallControlListener {
    fun notifyViewCancelCall()
}