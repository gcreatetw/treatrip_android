package com.gcreate.treatrip.listener

import java.util.concurrent.CopyOnWriteArrayList

class InterruptCallControlListenerManager {


    companion object {
        /**
         * 單例模式
         */
        var listenerManager: InterruptCallControlListenerManager? = null

        /**
         * 注册的接口集合，發送廣播的時候都能收到
         */
        private val iListenerList: MutableList<InterruptCallControlListener> = CopyOnWriteArrayList()

        /**
         * 獲得單例對象對象
         */
        fun getInstance(): InterruptCallControlListenerManager? {
            if (listenerManager == null) {
                listenerManager = InterruptCallControlListenerManager()
            }
            return listenerManager
        }

    }

    /**
     * 注册監聽
     */
    fun registerListener(iListener: InterruptCallControlListener) {
        iListenerList.add(iListener)
    }

    /**
     * 註銷監聽
     */
    fun unRegisterListener(iListener: InterruptCallControlListener) {
        if (iListenerList.contains(iListener)) {
            iListenerList.remove(iListener)
        }
    }


    /**
     * 發送廣播
     */
    fun sendBroadCast() {
        for (cancelCall in iListenerList) {
            cancelCall.notifyViewCancelCall()
        }
    }

}