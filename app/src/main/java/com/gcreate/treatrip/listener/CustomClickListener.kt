package com.gcreate.treatrip.listener

import android.view.View
import java.util.*

/*  確保元件短時間無法連續點擊
* */
internal abstract class CustomClickListener : View.OnClickListener {
    private var lastClickTime: Long = 0
    private var id = -1
    override fun onClick(v: View) {
        val currentTime: Long = Calendar.getInstance().timeInMillis
        val mId: Int = v.id
        if (id != mId) {
            id = mId
            lastClickTime = currentTime
            onNoDoubleClick(v)
            return
        }
        if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime
            onNoDoubleClick(v)
        }
    }

    protected abstract fun onNoDoubleClick(v: View?)

    companion object {
        private const val MIN_CLICK_DELAY_TIME = 1000 // 设置1秒内只能点击一次
    }
}