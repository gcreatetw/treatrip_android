package com.gcreate.treatrip.listener;

public interface UpdateNameListener {
    void notifyUserName(String cityName);
}
