package com.gcreate.treatrip.listener

interface ItemClickListener {
    fun onItemClickListener(position: Int)

}