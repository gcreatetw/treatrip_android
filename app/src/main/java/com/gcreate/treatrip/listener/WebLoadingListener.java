package com.gcreate.treatrip.listener;

public interface WebLoadingListener {
    void notifyViewCancelCall();
}
