@file:Suppress("UNCHECKED_CAST")

package com.gcreate.treatrip.tools

import android.os.Bundle
import androidx.fragment.app.Fragment
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class Arg<T>(val name: String , private val defaultValue: T) : ReadWriteProperty<Fragment , T> {
    override fun getValue(thisRef: Fragment , property: KProperty<*>): T {
        if (thisRef.arguments == null) {
            return defaultValue
        }

        return when (defaultValue) {
            is String -> thisRef.arguments?.getString(name , defaultValue) as T
            is Int -> thisRef.arguments?.getInt(name , defaultValue) as T
            is Boolean -> thisRef.arguments?.getBoolean(name , defaultValue) as T
            is Long -> thisRef.arguments?.getLong(name , defaultValue) as T
            else -> throw RuntimeException()
        }
    }

    override fun setValue(thisRef: Fragment , property: KProperty<*> , value: T) {
        if (thisRef.arguments == null) {
            thisRef.arguments = Bundle()
        }

        when (value) {
            is String -> thisRef.arguments?.putString(name , value)
            is Int -> thisRef.arguments?.putInt(name , value)
            is Boolean -> thisRef.arguments?.putBoolean(name , value)
            is Long -> thisRef.arguments?.putLong(name , value)
            else -> throw RuntimeException()
        }
    }
}
