package com.gcreate.treatrip.tools

import android.app.Activity

@Suppress("UNCHECKED_CAST")
fun <T> Activity.lazyExtra(name: String , defaultValue: T): Lazy<T> {
    return lazy {
        when (defaultValue) {
            is String -> intent.getStringExtra(name) as T ?: defaultValue
            is Int -> intent.getIntExtra(name , defaultValue) as T
            is Boolean -> intent.getBooleanExtra(name , defaultValue) as T
            else -> throw RuntimeException()
        }

    }
}
