package com.gcreate.treatrip.tools

import android.app.Activity
import android.util.DisplayMetrics
import com.gcreate.treatrip.webAPI.WebAPI
import com.gcreate.treatrip.webAPI.WebAPI.Companion.TREATRIP_SERVER
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class Global {


    companion object {
        var windowWidth = 0
        var windowHeight = 0
        lateinit var treatripAPI: WebAPI



        @JvmName("getWindowSize")
        @JvmStatic
        fun getWindowSize(mActivity: Activity) {
            val dm = DisplayMetrics()
            mActivity.windowManager.defaultDisplay.getMetrics(dm)
            windowHeight = dm.heightPixels
            windowWidth = dm.widthPixels
        }

        fun initTreatripAPI() {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor { chain ->
                val builder = chain.request().newBuilder()
                val build = builder.build()
                chain.proceed(build)
            }.connectTimeout(60 , TimeUnit.SECONDS).readTimeout(60 , TimeUnit.SECONDS).build()

            treatripAPI = Retrofit.Builder().baseUrl(TREATRIP_SERVER).addConverterFactory(GsonConverterFactory.create()).client(client).build().create(WebAPI::class.java)

        }


    }


}